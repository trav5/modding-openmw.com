##########################
# Gonzo's MOMW Git/Website Tool
# v. 1.3
#
# Pre-requisites:
#  - wsl -d Ubuntu
#  - sudo apt install make python3 python3-pip python-is-python3 libxml2-dev
#
##########################

# Main Menu
function ShowMenu {
	Clear-Host
	Write-Host "Welcome to Gonzo's MOMW Git/Website Tool:"
	Write-Host "(S)witch Branch - Checkout a different branch"
	Write-Host "(U)pdate Files - Pull changes down from a branch"
	Write-Host "(O)pen Directory - Opens the working directory"
	Write-Host "(R)eset Server - Reset the local server"
	Write-Host "(C)ommit Changes - Run Python Black and Stage files with Git"
	Write-Host "(P)ush Commits - Fetch, Rebase, and Push to a branch"
	Write-Host "(Q)uit - Exit the script"
	$prompt = Read-Host "Enter Command"
	# Valid menu commands:
	switch ($prompt) {
		"s" {SwitchBranch}
		"S" {SwitchBranch}
		"u" {UpdateFiles}
		"U" {UpdateFiles}
		"o" {OpenDirectory}
		"O" {OpenDirectory}
		"r" {ResetServer}
		"R" {ResetServer}
		"c" {CommitFiles}
		"C" {CommitFiles}
		"p" {PushCommit}
		"P" {PushCommit}
		"q" {EndProgram}
		"Q" {EndProgram}
		Default {
			Write-Host "Option Not Available"
			Start-Sleep -Seconds 2
			ShowMenu
		}
	}
}

# SwitchBranch - Checkout a given branch
function SwitchBranch {
	cd "$PSScriptRoot\.."
	$branch = Read-Host "Enter target branch"
	git checkout $branch
	Write-Host "Returning to menu..."
	Start-Sleep -Seconds 3
	ShowMenu
}

# UpdateFiles - Pull changes down from the current branch
function UpdateFiles {
	cd "$PSScriptRoot\.."
	git pull
	Write-Host "Returning to menu..."
	Start-Sleep -Seconds 3
	ShowMenu
}

# OpenDirectory - Opens the working directory
function OpenDirectory {
	Clear-Host
	Explorer.exe "$PSScriptRoot\.."
	ShowMenu
}

# ResetServer - Reset the local server
function ResetServer {
	cd "$PSScriptRoot\.."
	Write-Host "Loading Server. Please wait..."
	
	# Make the server in the Ubuntu WSL instance -- the Happy Path (tm)
	wsl -u root -d Ubuntu --exec bash -c "rsync --delete -aPv ../modding-openmw.com/ ~/modding-openmw.com/ && cd ~/modding-openmw.com/ && make ub-venv pip-requirements reset server"
	
	Read-Host -Prompt "Press Enter to return to the main menu or CTRL+C to quit" | Out-Null
	Start-Sleep -Seconds 1
	ShowMenu
}

# CommitFiles - Run Python Black, add files to git, and commit with message
function CommitFiles {
	cd "$PSScriptRoot\.."
	$message = Read-Host "Enter commit message"
	
	# Run Python Black
	python -m black -t py37 --exclude migrations .
	
	# Add file changes and file deletions to git
	git add -A
	
	# Commit changes using the provided message
	git commit -m "$message"

	Read-Host -Prompt "Press Enter to return to the main menu or CTRL+C to quit" | Out-Null
	Start-Sleep -Seconds 1
	ShowMenu
}

# PushCommit - Use Git to fetch, rebase, and push to a given branch
function PushCommit {
	cd "$PSScriptRoot\.."
	$branch = Read-Host "Enter target branch"
	git fetch origin $branch
	git rebase -i origin/$branch
	Read-Host -Prompt "Push Commits? Press Enter to continue or CTRL+C to quit" | Out-Null
	git push origin $branch
	Write-Host "Pushed commits to $branch"
	Start-Sleep -Seconds 3
	ShowMenu
}

# EndProgram - Reset the Docker container and close the script
function EndProgram {
	Write-Host "Cleaning database file..."
	wsl -u root -d Ubuntu --exec bash -c "rm ~/modding-openmw.com/momw/momw-db.sqlite3"
	Write-Host "Exiting the script..."
	Start-Sleep -Seconds 1
	cls
	Exit
}
ShowMenu
