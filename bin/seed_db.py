#!/usr/bin/env python3
import logging
import sys
import os
import pytz

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BASE_DIR + "/momw")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "momw.settings")
import django

django.setup()
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from momw.data_seeds.blog import (
    blog_category_factory,
    blog_tag_factory,
    blog_entry_factory,
)
from momw.data_seeds.changelogs import changelogs
from momw.data_seeds.media import media_factory
from momw.data_seeds.mod_categories import mod_cats
from momw.data_seeds.mod_datapaths import data_paths
from momw.data_seeds.mod_data import mods
from momw.data_seeds.mod_lists import generate_strings, mod_lists
from momw.data_seeds.mod_plugin_order import plugins_load_order
from momw.data_seeds.mod_tags import mod_tags
from momw.data_seeds.usage_notes import usage_notes
from momw.data_seeds.extra_cfg import extra_cfg


TZ = pytz.timezone(settings.TIME_ZONE)
User = get_user_model()


def site_factory():
    s1 = Site.objects.get(pk=1)
    s1.domain = settings.PROJECT_HOSTNAME
    s1.name = settings.SITE_NAME
    s1.save()


def user_factory():
    """Creates user accounts!"""
    admin = {
        "username": "MaiqTheAdmin",
        "password": "pbkdf2_sha256$30000$sq1bF20gppnd$OLo3HeVIpZXD9obg4uFU3SjbHQE4kCAV0Pmgo6cqNrw=",
        "first_name": "M'aiq",
        "last_name": "The Admin",
        "email": "admin@modding-openmw.com",
    }
    a = User(**admin)
    a.is_staff = True
    a.is_superuser = True
    a.save()


def main():
    logging.basicConfig(
        format="%(asctime)s : %(message)s", level=logging.INFO, stream=sys.stdout
    )
    logger = logging.getLogger()
    info = logger.info

    def run(func, earg=None):
        info("Running {}() ...".format(func.__name__))
        if earg:
            func(earg)
        else:
            func()

    info("Starting {} db seed ...".format(settings.PROJECT_NAME.upper()))
    run(site_factory)
    run(user_factory)
    run(blog_category_factory)
    run(blog_tag_factory)
    run(blog_entry_factory)
    run(media_factory)
    run(mod_cats)
    run(mod_tags)
    run(mods)
    run(mod_lists)
    run(plugins_load_order)
    run(data_paths)
    run(generate_strings)
    run(usage_notes)
    run(extra_cfg)
    run(changelogs)
    info("Finished {} db seed!".format(settings.PROJECT_NAME.upper()))


if __name__ == "__main__":
    main()
