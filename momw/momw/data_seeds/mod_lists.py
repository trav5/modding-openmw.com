import os
import sys

from momw.helpers import make_slug, read_toml_data, read_yaml_data
from momw.models import ListedMod, Mod, ModList, SubList


def add_mod_to_list(modlist: ModList, mod: Mod, order_number: int) -> ListedMod:
    """
    Add a Mod to a ModList and create the associated ListedMod object.

    Return the ListedMod object, for fun.
    """
    lm = ListedMod(mod=mod, modlist=modlist, order_number=order_number)
    lm.save()
    # print("ADDED:", lm)
    return lm


def generate_mod_list_obj(parent_slug=None, **kwargs) -> ModList:
    """
    A wrapper for instantiating a ModList object.

    Accepts a dict of things to fill the object's tables with.
    """
    slug = make_slug(kwargs["title"])
    kwargs.update({"slug": slug})
    mlist = ModList(**kwargs)
    mlist.save()
    return mlist


def generate_list_with_sublists(toml_data) -> ModList:
    """
    Given properly formed 'toml_data':

    In [8]: read_toml_data("list-with-sublists")
    Out[8]:
    {'title': 'Testing Sublists',
     'description': 'todo description',
     'short_description': 'todo short_description',
     'sublists': [{'title': 'Testing Sublists',
       'description': 'todo description',
       'short_description': 'todo short_description',
       'mods': ['foo1', 'bar1', 'baz1']},
      {'title': 'Testing Sublists 2',
       'description': 'todo description 2',
       'short_description': 'todo short_description 2',
       'mods': ['foo2', 'bar2', 'baz2']}]}

    Generate a ModList and any "sublists".

    """
    # Generate the parent...
    parent_obj = generate_mod_list_obj(
        **{
            "title": toml_data["title"],
            "description": toml_data["description"],
            "short_description": toml_data["short_description"],
            "status": ModList.LIVE,
        }
    )

    # Then each sublist.
    count = 1
    for sublist in toml_data["sublists"]:
        short_desc = sublist.get("short_description", None)
        if not short_desc:
            short_desc = sublist["description"][24:] + "…"
        # Generate the list object first...
        modlist_obj = generate_mod_list_obj(
            parent_slug=parent_obj.slug,
            **{
                "title": "{0}: {1}".format(toml_data["title"], sublist["title"]),
                "description": sublist["description"],
                "short_description": short_desc,
                "status": ModList.LIVE,
            },
        )
        modlist_obj.parent_list = parent_obj
        modlist_obj.save()

        # Then populate it with mods.
        generate_list(sublist["mods"], modlist_obj)

        # SubList object
        sub_obj = SubList(
            modlist=modlist_obj, parent_list=parent_obj, order_number=count
        )
        sub_obj.save()
        count += 1
    return parent_obj


def generate_list(list_list: list, list_obj: ModList, num=1) -> ModList:
    """
    Given the Mod list 'list_list' and the ModList object 'list_obj',
    add the Mod to the ModList and increment the step number 'num' by one.

    'list_list' is a list of mod names.

    Return the ModList object.
    """
    no_matches = []
    if not list_list:
        # TODO: When does this even happen... and why?!
        # Maybe tests..
        return list_obj

    for s in list_list:
        m = None
        try:
            # First: lookup by slug
            m = Mod.objects.get(slug=s)

        except Mod.DoesNotExist:
            try:
                # Second: lookup by name
                m = Mod.objects.get(name=s)

            except Mod.DoesNotExist:
                # Third: try to case-insensitive search names and slugs for the
                # given string, then print an error if that fails
                maybe = Mod.objects.filter(name__icontains=s, slug__icontains=s)
                if len(maybe) == 0:
                    if not os.getenv("MOMW_TESTING"):
                        print("ERROR: Unable to find an exact match for:", s)
                    no_matches.append(s)
                    # print("WARNING: Possible matches found:")
                    # print("-" * 35)
                    # for m in maybe:
                    #     print(m)
                    # print("-" * 35)
                else:
                    # print("ERROR: Unable to find a mod that matches:", s)
                    pass

        if m:
            add_mod_to_list(list_obj, m, num)
            num += 1

    if len(no_matches) > 0 and not os.getenv("MOMW_TESTING"):
        print("BAD MOD NAMES FOUND! ERRORING OUT....")
        sys.exit(1)

    return list_obj


def generate_strings():
    for modlist in ModList.objects.all():
        linux_str = ""
        windows_str = ""
        sublists = modlist.modlist_set.all()
        if sublists:
            for sublist in sublists:
                mods = ListedMod.objects.filter(modlist=sublist)
                for m in mods:
                    mod = m.mod
                    if mod.category.slug == "settings-tweaks":
                        continue
                    path_added = False
                    for dp in mod.data_paths.filter(on_lists=sublist.parent_list):
                        if dp.manual:
                            linux_str += f"mkdir -p '{dp.linux}'\n"
                            windows_str += (
                                f"New-Item -ItemType 'directory' -Path '{dp.windows}'\n"
                            )
                            path_added = True
                    if not path_added:
                        linux_str += f"mkdir -p '{mod.get_moddir_linux}'\n"
                        windows_str += f"New-Item -ItemType 'directory' -Path '{mod.get_moddir_windows}'\n"

        elif not modlist.is_sublist:
            mods = ListedMod.objects.filter(modlist=modlist)
            for m in mods:
                mod = m.mod
                if mod.category.slug == "settings-tweaks":
                    continue
                path_added = False
                for dp in mod.data_paths.filter(on_lists=modlist):
                    if dp.manual:
                        linux_str += f"mkdir -p '{dp.linux}'\n"
                        windows_str += (
                            f"New-Item -ItemType 'directory' -Path '{dp.windows}'\n"
                        )
                        path_added = True
                if not path_added:
                    linux_str += f"mkdir -p '{mod.get_moddir_linux}'\n"
                    windows_str += f"New-Item -ItemType 'directory' -Path '{mod.get_moddir_windows}'\n"

        modlist.linux_str = linux_str
        modlist.windows_str = windows_str
        modlist.save()


def set_parent_orders(parent_list: ModList):
    """
    It's too expensive to do queries to figure out these things,
    so we set them as fields and compute them at DB seed-time.
    """
    for modlist in parent_list.modlist_set.all():
        for mod in modlist.listedmod_set.all():
            sl = SubList.objects.get(modlist=modlist)
            if sl.order_number > 1:
                prev_lists_count = 0
                for num in range(parent_list.modlist_set.count()):
                    if num + 1 == sl.order_number:
                        break

                    prev_lists_count += parent_list.modlist_set.all()[num].mod_count

                mod.parent_order = mod.order_number + prev_lists_count
                # print("ORDER: " + str(mod.parent_order))
            else:
                # Must set parent order as order so that step detail works!
                mod.parent_order = mod.order_number

            mod.parent_slug = parent_list.slug
            mod.save()


def mod_lists():
    """
    Create all mod lists.
    """
    #
    # Lists with lists!
    #
    # First, create the list objects:
    # Monolithic lists
    i_heart_vanilla_obj = generate_mod_list_obj(
        **{
            "description": """<p>This list is for those that want to play the vanilla game, but with a few quality of life improvements.</p>

<p>No new content is added, the only things changed by plugins are to fix bugs and meshes/textures are replaced to add gameplay features like Graphic Herbalism and the like.</p>""",
            "short_description": "This list is for those that want to play the vanilla game, but with a few quality of life improvements.",
            "status": ModList.LIVE,
            "title": "I Heart Vanilla",
        }
    )

    i_heart_vanilla_dc_obj = generate_mod_list_obj(
        **{
            "description": """<p>This is an extension of the "I Heart Vanilla" list that includes Tamriel Rebuilt and related mods, and unofficial mods with content from original Morrowind creators.</p>

<p>Vanilla, but with a little bit more!  The sheer amount of content that's added with just a few extra mods is indeed staggering. The list also features new quests from one of the writers of the original Morrowind!</p>""",
            "short_description": 'This is an extension of the "I Heart Vanilla" list that includes Tamriel Rebuilt and related mods to greatly expand the available landmasses, with other goodies.',
            "status": ModList.LIVE,
            "title": "I Heart Vanilla: Director's Cut",
        }
    )

    jgm_obj = generate_mod_list_obj(
        **{
            "description": """<p>Limited but impactful graphical mods, performance optimizations, many gameplay mods, and no extra content or quests. Just Good Morrowind.</p><p>The vanilla Morrowind textures with normal maps look surprisingly nice for what they are. Combine that with the overwhelming beauty provided by various shaders and what you get is a simple yet enjoyable Morrowind experience, with gameplay changes and improvements to really spice things up. This mod list was optimized for maximum FPS on a Steam Deck.</p>

<p>This is not intended for a first time player, instead check out I Heart Vanilla.</p>""",
            "short_description": "Limited but impactful graphical mods, performance optimizations, many gameplay mods, and no extra content or quests. Just Good Morrowind.",
            "status": ModList.LIVE,
            "title": "Just Good Morrowind",
        }
    )

    oneday_modernization = read_toml_data("1day-modernization")
    oneday_modernization_obj = generate_list_with_sublists(oneday_modernization)

    graphics_overhaul_with_sublists_list = read_toml_data("graphics-overhaul")
    graphics_overhaul_obj = generate_list_with_sublists(
        graphics_overhaul_with_sublists_list
    )

    expanded_vanilla_with_sublists_list = read_toml_data("expanded-vanilla")
    expanded_vanilla_obj = generate_list_with_sublists(
        expanded_vanilla_with_sublists_list
    )

    total_overhaul_with_sublists_list = read_toml_data("total-overhaul")
    total_overhaul_obj = generate_list_with_sublists(total_overhaul_with_sublists_list)

    # Then, populate extra data:
    set_parent_orders(ModList.objects.get(id=total_overhaul_obj.id))
    set_parent_orders(ModList.objects.get(id=graphics_overhaul_obj.id))
    set_parent_orders(ModList.objects.get(id=expanded_vanilla_obj.id))
    set_parent_orders(ModList.objects.get(id=oneday_modernization_obj.id))

    tes3mp_server_obj = generate_mod_list_obj(
        **{
            "description": """<p>This list has been discontinued.</p>""",
            "short_description": """<p>This list has been discontinued.</p>""",
            "status": ModList.LIVE,
            "title": "TES3MP Server",
        }
    )

    starwind_obj = generate_mod_list_obj(
        **{
            "description": """<p>A mod list centered around the Total Conversion Starwind, adding various features and extra content. Join <a href="https://discord.gg/WFBvU6NVCN">the official Starwind Discord server</a> today!</p>""",
            "short_description": "A mod list centered around the Total Conversion Starwind, adding various features and extra content.",
            "status": ModList.LIVE,
            "title": "Starwind: Modded",
        }
    )
    i_heart_vanilla_list = read_yaml_data(i_heart_vanilla_obj.slug)
    i_heart_vanilla_dc_list = read_yaml_data("i-heart-vanilla-dc")
    tes3mp_server_list = read_yaml_data(tes3mp_server_obj.slug)
    starwind_list = read_yaml_data(starwind_obj.slug)
    jgm_list = read_yaml_data(jgm_obj.slug)

    generate_list(i_heart_vanilla_list, i_heart_vanilla_obj)
    generate_list(i_heart_vanilla_dc_list, i_heart_vanilla_dc_obj)
    generate_list(tes3mp_server_list, tes3mp_server_obj)
    generate_list(starwind_list, starwind_obj)
    generate_list(jgm_list, jgm_obj)
