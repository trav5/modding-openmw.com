import pytz

from datetime import datetime
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.text import slugify
from media.models import MediaTrack
from momw.helpers import make_slug

TZ = pytz.timezone(settings.TIME_ZONE)
User = get_user_model()


def media_factory():
    datetime_format = "%Y-%m-%d %H:%M:%S %z"
    u = User.objects.get(pk=1)

    # MediaTrack.objects.create(
    #     author=u, description="DESC",
    #     title="TITLE", url="/images/FILE", slug="SLUG",
    #     media_type=MediaTrack.IMAGE, status=MediaTrack.LIVE,
    #     date_added=datetime.strptime('2018-07-04 18:00:00 -0500', datetime_format)).save()

    # TODO: need one for the expanded vanilla intro video
    MediaTrack.objects.create(
        author=u,
        description="""<h3>Specs:</h3>
<ul>
  <li>OpenMW 0.48.0</li>
  <li>Linux 6.0.10-tkg-bmq-zen (<a href="https://voidlinux.org/">Void Linux</a>)</li>
  <li>AMD Ryzen 5 1600</li>
  <li>AMD Radeon RX 5700 XT</li>
  <li>Mesa 21.2.1</li>
  <li>32GB system RAM</li>
  <li>Gameplay captured with <a href="https://obsproject.com/">OBS Studio</a></li>
  <li>Video created with <a href="https://www.blender.org/">Blender</a></li>
</ul>""",
        other_html="""<div class="center"><iframe width="560" height="315" src="https://www.youtube.com/embed/UvPPn5UHH2c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>""",
        title="Expanded Vanilla: Intro",
        url="https://www.youtube.com/watch?v=UvPPn5UHH2c",
        slug=make_slug("Expanded Vanilla: Intro"),
        media_type=MediaTrack.VIDEO,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2022-12-06 16:51:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="""<h3>Specs:</h3>

<ul>
  <li>OpenMW 0.48.0</li>
  <li>Linux 6.0.9-tkg-bmq-zen (<a href="https://voidlinux.org/">Void Linux</a>)</li>
  <li>AMD Ryzen 5 1600</li>
  <li>AMD Radeon RX 5700 XT</li>
  <li>Mesa 21.2.1</li>
  <li>32GB system RAM</li>
  <li>Gameplay captured with <a href="https://obsproject.com/">OBS Studio</a></li>
  <li>Video created with <a href="https://www.blender.org/">Blender</a></li>
</ul>""",
        other_html="""<div class="center"><iframe width="560" height="315" src="https://www.youtube.com/embed/TQzCiYzfXP0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>""",
        title="A Total Overhaul Tour: OpenMW 0.48.0 | 2022-11 Edition",
        url="https://www.youtube.com/watch?v=TQzCiYzfXP0",
        slug=make_slug("A Total Overhaul Tour: OpenMW 0.48.0 | 2022-11 Edition"),
        media_type=MediaTrack.VIDEO,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2022-11-26 14:32:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="The complete Total Overhaul mod list running on Valve's Steam Deck.",
        title="Total Overhaul Mod List on Steam Deck",
        url="/images/steamdeck-openmw-totaloverhaul.jpg",
        slug=make_slug("Total Overhaul Mod List on Steam Deck"),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2022-11-26 14:31:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="""<p class="bold">Special thanks to <a href="https://mw.moddinghall.com/">Morrowind Modding Hall</a> for hosting this video!</p>

<p>A non-exhaustive look at what your game will be like after following the 358-mod (as of this recording) <a href="/lists/total-overhaul/">Total Overhaul list</a>. This iteration of the tour focuses on features in the new OpenMW 0.47.0 release, as well as new mod additions on this website:</p>

<ul>
  <li>Intro: <a class="time-link" data-time="0">0:00</a></li>
  <li><a href="/mods/seyda-neen-gateway-to-vvardenfell/">Seyda Neen</a>: <a class="time-link" data-time="10">00:10</a></li>
  <li><a href="/mods/oaab-the-ashen-divide/">The Ashen Divide</a>: <a class="time-link" data-time="127">02:07</a></li>
  <li><a href="/mods/hla-oad/">Hla Oad</a>: <a class="time-link" data-time="282">04:42</a></li>
  <li><a href="/mods/aldruhn/">Ald'ruhn</a>: <a class="time-link" data-time="402">06:42</a></li>
  <li><a href="/mods/rr-mod-series-ghostgate-fortress/">Ghostgate Fortress</a>: <a class="time-link" data-time="550">09:10</a></li>
  <li>Molag Amur (with <a href="/mods/oaab-dwemer-pavements/">Dwemer Pavements</a> and <a href="/mods/i-lava-good-mesh-replacer/">I Lava Good</a>): <a class="time-link" data-time="631">10:31</a></li>
  <li>Vos (<a href="/mods/oaab-grazelands/">OAAB Grazelands</a>): <a class="time-link" data-time="755">12:35</a></li>
</ul>

<h3>Specs:</h3>

<ul>
  <li>OpenMW 0.47.0</li>
  <li>Linux 5.13.18-tkg-bmq-zen (<a href="https://voidlinux.org/">Void Linux</a>)</li>
  <li>AMD Ryzen 5 1600</li>
  <li>AMD Radeon RX 5700 XT</li>
  <li>Mesa 21.2.1</li>
  <li>32GB system RAM</li>
  <li>Gameplay captured with <a href="https://obsproject.com/">OBS Studio</a></li>
  <li>Video created with <a href="https://www.blender.org/">Blender</a></li>
</ul>

<a href="https://www.youtube.com/watch?v=8WmU-lHVTwc" title="I will never display ads on any of my videos or websites, but I can't guarantee that the video won't suddenly be 'copyright claimed' and have ads forced on it.  Thus, the youtube link isn't really heavily promoted or encouraged unless you're having issues with the one I host being slow.">Watch this on youtube</a>.""",
        title="A Total Overhaul Tour: OpenMW 0.47.0 | 2021-09 Edition",
        url="https://mwmoddinghall.s3.ap-southeast-1.amazonaws.com/videos/to-tour-2021-09-scaled.mp4",
        slug=make_slug("A Total Overhaul Tour: OpenMW 0.47.0 | 2021-09 Edition"),
        poster="/images/to-tour-2021-09.png",
        media_type=MediaTrack.VIDEO,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2021-11-05 12:08:39 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="""<p>A non-exhaustive look at what your game will be like after following the 253 mod (as of this recording) <a href="/lists/total-overhaul/">Total Overhaul list</a>, updated to reflect all the new goodies added in the last six-plus months:</p>

<ul>
  <li>Intro: <a class="time-link" data-time="0">0:00</a></li>
  <li>Pelagiad: <a class="time-link" data-time="12">00:12</a></li>
  <li>Pelagiad at night: <a class="time-link" data-time="93">01:33</a></li>
  <li>Balmora: <a class="time-link" data-time="117">01:57</a></li>
  <li>Caldera: <a class="time-link" data-time="216">03:36</a></li>
  <li>Ald-Ruhn: <a class="time-link" data-time="287">04:47</a></li>
  <li>Vos: <a class="time-link" data-time="380">06:20</a></li>
  <li>Suran: <a class="time-link" data-time="465">07:45</a></li>
  <li>Addamasartus: <a class="time-link" data-time="543">09:03</a></li>
  <li>Molag Mar: <a class="time-link" data-time="600">10:00</a></li>
  <li>Ghostgate: <a class="time-link" data-time="702">11:43</a></li>
  <li>Port Telvannis: <a class="time-link" data-time="769">12:50</a></li>
  <li>Sadrith Mora: <a class="time-link" data-time="840">14:01</a></li>
  <li>Arkngthand: <a class="time-link" data-time="954">15:55</a></li>
  <li>Palace of Vivec: <a class="time-link" data-time="1013">16:54</a></li>
  <li>Dagoth Ur: <a class="time-link" data-time="1053">17:33</a></li>
  <li>Fort Frostmoth: <a class="time-link" data-time="1076">17:56</a></li>
</ul>

<p>Note that in the video I've got the Tamriel Rebuilt Preview plugins enabled in order to fill out the distant land; the Total Overhaul list does not advise this setup by default but you can easily opt into it. Look for instructions in the <a href="/mods/tamriel-rebuilt/">Tamriel Rebuilt</a> usage notes..</p>

<h3>Specs:</h3>

<ul>
  <li>OpenMW 0.47.0 dev (rev: <code>f8c068ee347d031fa810d22460bec99ad51a8859</code>)</li>
  <li>Linux 5.10.13-tkg-muqss-zen</li>
  <li>AMD Ryzen 5 1600</li>
  <li>AMD Radeon RX 5700 XT</li>
  <li>Mesa 20.3.4</li>
  <li>32GB system RAM</li>
</ul>

<a href="https://www.youtube.com/watch?v=j02o80IRagU" title="I will never display ads on any of my videos or websites, but I can't guarantee that the video won't suddenly be 'copyright claimed' and have ads forced on it.  Thus, the youtube link isn't really heavily promoted or encouraged unless you're having issues with the one I host being slow.">Watch this on youtube</a>.""",
        title="A Total Overhaul Tour: 2021-02 Edition",
        url="/videos/to-tour-2021-02.mp4",
        slug="to-tour-2021-02",
        poster="/images/to-tour-2021-02.png",
        media_type=MediaTrack.VIDEO,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2021-02-05 06:44:29 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="""<p class="bold">This video has been superceded! Check out the new version <a href="/media/to-tour-2021-02">here</a>.</p>

<p>Take a look at what your game will look like after following the 263-mod (as of this recording) <a href="/lists/total-overhaul/">Total Overhaul list</a>.</p>

<ul>
  <li>A new game: <a class="time-link" data-time="0">0:00</a></li>
  <li>Pelagiad: <a class="time-link" data-time="633">10:33</a></li>
  <li>Pelagiad at night: <a class="time-link" data-time="777">12:57</a></li>
  <li>Balmora: <a class="time-link" data-time="820">13:40</a></li>
  <li>Caldera: <a class="time-link" data-time="1135">18:55</a></li>
  <li>Ald-Ruhn: <a class="time-link" data-time="1214">20:14</a></li>
  <li>Vos: <a class="time-link" data-time="1332">22:12</a></li>
  <li>Suran: <a class="time-link" data-time="1400">23:20</a></li>
  <li>Addamasartus: <a class="time-link" data-time="1525">25:25</a></li>
  <li>Molag Mar: <a class="time-link" data-time="1624">27:04</a></li>
  <li>Ghostgate: <a class="time-link" data-time="1754">29:14</a></li>
  <li>Mournhold: <a class="time-link" data-time="1855">30:55</a></li>
  <li>Sadrith Mora: <a class="time-link" data-time="2027">33:47</a></li>
  <li>Tureynulal: <a class="time-link" data-time="2289">38:09</a></li>
  <li>Palace of Vivec: <a class="time-link" data-time="2317">38:37</a></li>
  <li>Solstheim: Tomb of the Snow Prince: <a class="time-link" data-time="2340">39:00</a></li>
</ul>

<h3>Specs:</h3>

<ul>
  <li>OpenMW 0.47.0 dev (rev: e98ae3262e4d5cc5611e26b303e9120bd879306b)</li>
  <li>Linux 5.7.8</li>
  <li>AMD Ryzen 5 1600</li>
  <li>AMD Radeon RX 5700 XT</li>
  <li>Mesa 20.1.2</li>
  <li>32GB system RAM</li>
</ul>

<a href="https://www.youtube.com/watch?v=4G2wvlGw05Y" title="I will never display ads on any of my videos or websites, but I can't guarantee that the video won't suddenly be 'copyright claimed' and have ads forced on it.  Thus, the youtube link isn't really heavily promoted or encouraged unless you're having issues with the one I host being slow.">Watch this on youtube</a>.""",
        title="A Total Overhaul Tour",
        url="/videos/total-overhaul-tour.mp4",
        slug="total-overhaul-tour",
        poster="/images/total-overhaul-tour.png",
        media_type=MediaTrack.VIDEO,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2020-07-15 12:58:50 -0500", datetime_format),
    ).save()

    __title = "Total Overhaul: Welcome To Seyda Neen"
    MediaTrack.objects.create(
        author=u,
        description="""Being welcomed off the prison ship in a Total Overhaul new game.""",
        title=__title,
        url="/images/welcome-to-seyda-neen.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:54:00 -0500", datetime_format),
    ).save()

    __title = "A Chicken In Skyrim"
    MediaTrack.objects.create(
        author=u,
        description="""The wildlife of Skyrim.  From <a href="/mods/skyrim-home-of-the-nords/">Skyrim: Home Of The Nords</a>.""",
        title=__title,
        url="/images/skyrim-chicken.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:52:00 -0500", datetime_format),
    ).save()

    __title = "Sixth House Rises: Part 3"
    MediaTrack.objects.create(
        author=u,
        description="""The cult paints the town.  From <a href='/mods/main-quest-enhancers/'>Main Quest Enhancers</a>/<a href='/mods/main-quest-enhancers-patch/'>Main Quest Enhancers Patch</a>.""",
        title=__title,
        url="/images/sixth-house-rises-mq-enhancer3.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:50:00 -0500", datetime_format),
    ).save()

    __title = "Sixth House Rises: Part 2"
    MediaTrack.objects.create(
        author=u,
        description="""The Sixth House presense is becoming more noticeable...  From <a href='/mods/main-quest-enhancers/'>Main Quest Enhancers</a>/<a href='/mods/main-quest-enhancers-patch/'>Main Quest Enhancers Patch</a>.""",
        title=__title,
        url="/images/sixth-house-rises-mq-enhancer2.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:48:00 -0500", datetime_format),
    ).save()

    __title = "Sixth House Rises: Part 1"
    MediaTrack.objects.create(
        author=u,
        description="""A shot of some Sixth House materials that have shown up in Balmora as I progress through the main quest.  From <a href='/mods/main-quest-enhancers/'>Main Quest Enhancers</a>/<a href='/mods/main-quest-enhancers-patch/'>Main Quest Enhancers Patch</a>.""",
        title=__title,
        url="/images/sixth-house-rises-mq-enhancer1.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:48:00 -0500", datetime_format),
    ).save()

    __title = "Seyda Neen Sunset"
    MediaTrack.objects.create(
        author=u,
        description="""Another shot showing off the new shadows.  I find myself constantly stopping to admire them.  This was taken using all mods from <a href='/mods/list/'>The Total Overhaul List</a>.""",
        title=__title,
        url="/images/seyda-neen-sunset.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:46:00 -0500", datetime_format),
    ).save()

    __title = "Sunny Pelagiad"
    MediaTrack.objects.create(
        author=u,
        description="""The new shadows really add an extra level of detail to any scene.  This was taken using all mods from <a href='/mods/list/'>The Total Overhaul List</a>.""",
        title=__title,
        url="/images/pelagiad-sunny.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:44:00 -0500", datetime_format),
    ).save()

    __title = "Pelagiad Glows In The Dahrk"
    MediaTrack.objects.create(
        author=u,
        description="""Glowing night windows by <a href="/mods/glow-in-the-dahrk/">Glow in the Dahrk</a>.""",
        title=__title,
        url="/images/pelagiad-glows-in-the-dahrk.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:42:00 -0500", datetime_format),
    ).save()

    __title = "Our Favorite Enchanter"
    MediaTrack.objects.create(
        author=u,
        description="""Soul gems by <a href="/mods/soulgem-replacer/">Soulgem Replacer</a>.""",
        title=__title,
        url="/images/our-favorite-enchanter.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:40:00 -0500", datetime_format),
    ).save()

    __title = "Old Ebonheart"
    MediaTrack.objects.create(
        author=u,
        description="""A very large, very nice-looking city.  Unfortunately, it's so large that my FPS drops to single digits here.  Despite that, it's a joy to explore.  From <a href="/mods/tamriel-rebuilt/">Tamriel Rebuilt</a>.""",
        title=__title,
        url="/images/old-ebonheart.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:38:00 -0500", datetime_format),
    ).save()

    __title = "A New Shield Effect"
    MediaTrack.objects.create(
        author=u,
        description="""One of the many new spell effects provided by <a href="/mods/magic-diversity-complete-/">Magic Diversity -COMPLETE-</a>.""",
        title=__title,
        url="/images/new-shield-effect.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:36:00 -0500", datetime_format),
    ).save()

    __title = "[New] Ebonheart"
    MediaTrack.objects.create(
        author=u,
        description="""A shot of Ebonheart, showing off <a href="/mods/imperial-forts-normal-mapped-for-openmw/">Imperial Forts Normal Mapped for OpenMW</a>.""",
        title=__title,
        url="/images/new-ebonheart.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:34:00 -0500", datetime_format),
    ).save()

    __title = "Molag Amur Wilderness"
    MediaTrack.objects.create(
        author=u,
        description="""Wandering through the Molag Amur wilderness can be dangerous, yet beautiful.""",
        title=__title,
        url="/images/molag-amur-wilderness.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:32:00 -0500", datetime_format),
    ).save()

    __title = "Local Details"
    MediaTrack.objects.create(
        author=u,
        description="""Chatting with the locals in Pelagiad.""",
        title=__title,
        url="/images/local-details.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:30:00 -0500", datetime_format),
    ).save()

    __title = "Helnim: Part 3"
    MediaTrack.objects.create(
        author=u,
        description="""Walking around Helnim, part three.  From <a href="/mods/tamriel-rebuilt/">Tamriel Rebuilt</a>.""",
        title=__title,
        url="/images/helnim3.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:28:00 -0500", datetime_format),
    ).save()

    __title = "Helnim: Part 2"
    MediaTrack.objects.create(
        author=u,
        description="""Walking around Helnim, part two.  From <a href="/mods/tamriel-rebuilt/">Tamriel Rebuilt</a>.""",
        title=__title,
        url="/images/helnim2.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:26:00 -0500", datetime_format),
    ).save()

    __title = "Helnim: Part 1"
    MediaTrack.objects.create(
        author=u,
        description="""Walking around Helnim, part one.  From <a href="/mods/tamriel-rebuilt/">Tamriel Rebuilt</a>.""",
        title=__title,
        url="/images/helnim1.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:24:00 -0500", datetime_format),
    ).save()

    __title = "HD Main Menu"
    MediaTrack.objects.create(
        author=u,
        description="""Provided by <a href="/mods/hd-concept-art-splash-screen-and-main-menu/">HD Concept-art splash screen and main menu</a>.""",
        title=__title,
        url="/images/hd-main-menu.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:22:00 -0500", datetime_format),
    ).save()

    __title = "Rays Of Light"
    MediaTrack.objects.create(
        author=u,
        description="""More nice effects from <a href="/mods/glow-in-the-dahrk/">Glow in the Dahrk</a>.""",
        title=__title,
        url="/images/glow-in-the-dahrk-light-rays.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:20:00 -0500", datetime_format),
    ).save()

    __title = "Fancy Candles And Coin"
    MediaTrack.objects.create(
        author=u,
        description="""Candles by <a href="/mods/high-poly-candles-fixed/">High Poly Candles (fixed)</a>, coins by <a href="/mods/realistically-stacked-septims-and-gold-bags/">Realistically Stacked Septims and Gold Bags</a>.""",
        title=__title,
        url="/images/fancy-candles-and-coin.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:18:00 -0500", datetime_format),
    ).save()

    __title = "Close Encounters With The Evening Watch"
    MediaTrack.objects.create(
        author=u,
        description="""Greeted by the evening watch in Seyda Neen.""",
        title=__title,
        url="/images/evening-watch.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:16:00 -0500", datetime_format),
    ).save()

    __title = "Dwemer Puzzle Box: Normal Mapped"
    MediaTrack.objects.create(
        author=u,
        description="""Remiros' excellent <a href="/mods/dwemer-puzzle-box-replacer/">Dwemer Puzzle Box Replacer</a>.  Screw Hasphat, I'm keeping this!""",
        title=__title,
        url="/images/dwemer-puzzle-box-normal-map.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:14:00 -0500", datetime_format),
    ).save()

    __title = "Dirty Muriel's Glows In The Dahrk"
    MediaTrack.objects.create(
        author=u,
        description="""Dirty Muriels, looking very nice in the evening; glowing windows from <a href="/mods/glow-in-the-dahrk/">Glow in the Dahrk</a>.""",
        title=__title,
        url="/images/dirty-muriels-glow-in-the-dahrk.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:12:00 -0500", datetime_format),
    ).save()

    __title = "Conceptual Deity"
    MediaTrack.objects.create(
        author=u,
        description="""Vivec sporting an alternate look.  From <a href="/mods/concept-art-vivec-face-replacement/">Concept Art Vivec Face Replacement</a>.""",
        title=__title,
        url="/images/conceptual-vivec.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:12:00 -0500", datetime_format),
    ).save()

    __title = "Chickens In A Basket"
    MediaTrack.objects.create(
        author=u,
        description="""I went to Skyrim expecting to find wild things.  I got a basket of chickens.  From <a href="/mods/skyrim-home-of-the-nords/">Skyrim: Home Of The Nords</a>.""",
        title=__title,
        url="/images/chickens-in-a-basket.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:10:00 -0500", datetime_format),
    ).save()

    __title = "Bumpy Netches"
    MediaTrack.objects.create(
        author=u,
        description="""Extra-detailed netches from <a href="/mods/netch-bump-mapped/">Netch Bump Mapped</a>.""",
        title=__title,
        url="/images/bumpy-netches.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:08:00 -0500", datetime_format),
    ).save()

    __title = "Balmora: Night Sky"
    MediaTrack.objects.create(
        author=u,
        description="""Wandering through Balmora at night isn't so bad with a beautiful night sky, courtesy of <a href="/mods/skies-iv/">Skies .IV</a> and <a href="/mods/vurts-hi-res-skies-and-weathers/">Vurts Hi-res Skies and Weathers</a>.""",
        title=__title,
        url="/images/balmora-night.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:06:00 -0500", datetime_format),
    ).save()

    __title = "Balmora: A Dahrk Evening Part 2"
    MediaTrack.objects.create(
        author=u,
        description="""Another evening shot of Balmora, once again showing off <a href="/mods/glow-in-the-dahrk/">Glow in the Dahrk</a>.""",
        title=__title,
        url="/images/balmora-evening-glowing-in-the-dahrk.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:04:00 -0500", datetime_format),
    ).save()

    __title = "Balmora: A Dahrk Evening"
    MediaTrack.objects.create(
        author=u,
        description="""A beautiful night in Balmora, showing off <a href="/mods/glow-in-the-dahrk/">Glow in the Dahrk</a>.""",
        title=__title,
        url="/images/balmora-dahrk-evening.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:02:00 -0500", datetime_format),
    ).save()

    __title = "Azurian Bamboo"
    MediaTrack.objects.create(
        author=u,
        description="""A bamboo forest one can find in <a href="/mods/lyithdonea-the-azurian-isles/">Lyithdonea - The Azurian Isles</a>.""",
        title=__title,
        url="/images/azurian-bamboo.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2019-03-05 18:00:00 -0500", datetime_format),
    ).save()

    # __title = "A Total Overhaul Walk Through Kilcunda's Balmora"
    # MediaTrack.objects.create(
    #     author=u,
    #     description="""A walk through of Balmora with the Total Overhaul list applied, and OpenMW's newly added shadows!  A huge assortment of mods at play.""",
    #     title=__title,
    #     url="/videos/total-overhaul-walk-through-balmora.mp4",
    #     slug=slugify(__title),
    #     media_type=MediaTrack.VIDEO,
    #     status=MediaTrack.LIVE,
    #     date_added=datetime.strptime("2019-02-28 18:54:27 -0500", datetime_format),
    # ).save()

    # __title = "A Total Overhaul Walk Through Sadrith Mora Expanded"
    # MediaTrack.objects.create(
    #     author=u,
    #     description="""A nice walk through Sadrith Mora, with the entirety of the Total Overhaul list including <a href='/mods/sadrith-mora-expanded/'>Sadrith Mora Expanded</a>.  Add to this, a look at the newly added <a href='/mods/imperial-forts-normal-mapped-for-openmw/'>Imperial Forts Normal Mapped for OpenMW</a> with the new shadows!""",
    #     title=__title,
    #     url="/videos/total-overhaul-sadrith-mora-expanded.mp4",
    #     slug=slugify(__title)[:49],
    #     media_type=MediaTrack.VIDEO,
    #     status=MediaTrack.LIVE,
    #     date_added=datetime.strptime("2019-02-28 18:53:00 -0500", datetime_format),
    # ).save()

    # __title = "A Walk Through Mournhold Overhaul"
    # MediaTrack.objects.create(
    #     author=u,
    #     description="""A slow stroll through Mournhold.  Watch the result of the Total Overhaul list combined with the newly added shadows.  Also of note, a look at the newly-supported <a href='/mods/glow-in-the-dahrk/'></a> in action!""",
    #     title=__title,
    #     url="/videos/total-overhaul-walk-through-mournhold.mp4",
    #     slug=slugify(__title),
    #     media_type=MediaTrack.VIDEO,
    #     status=MediaTrack.LIVE,
    #     date_added=datetime.strptime("2019-02-28 18:50:00 -0500", datetime_format),
    # ).save()

    # __title = "Chargen Revamped: Bal Oyra to Firewatch"
    # MediaTrack.objects.create(
    #     author=u,
    #     description="""If you're like me and you've played Morrowind more than a few times, using the "Quick" startup from <a href="/mods/chargen-revamped-expanded-lands/">Chargen Revamped - Expanded Lands</a> is a pretty nice way to get the game going, and with a nice twist even.  In this video, I do a quick startup but randomize my spawn location.  I end up in Bal Oyra (<a href=\"/mods/tamriel-rebuilt/\">Tamriel Rebuilt</a>) and walk to Firewatch, where I poke around for a bit.""",
    #     title=__title,
    #     url="/videos/bal-oyra-to-firewatch.mp4",
    #     slug=slugify(__title),
    #     media_type=MediaTrack.VIDEO,
    #     status=MediaTrack.LIVE,
    #     date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    # ).save()

    # __title = "An Expanded Vanilla Intro"
    # MediaTrack.objects.create(
    #     author=u,
    #     description="""Featuring the 'Expanded Vanilla' preset and 'lo-fi' visual settings.  That original look and feel at a modern resolution, with <a href="/mods/pixelwind-v2/">Pixelwind V2</a> to spice it up a bit!  To me, this really shows how one can still enjoy that original look and feel even today.""",
    #     title=__title,
    #     url="/videos/expanded-vanilla-intro.mp4",
    #     slug=slugify(__title),
    #     media_type=MediaTrack.VIDEO,
    #     status=MediaTrack.LIVE,
    #     date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    # ).save()

    __title = "Inside Tel Darys"
    MediaTrack.objects.create(
        author=u,
        description='Just inside the lower entry to Tel Darys, in Gah Sadrith (<a href="/mods/tamriel-rebuilt/">Tamriel Rebuilt</a>.)',
        title=__title,
        url="/images/inside-tel-darys.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    ).save()

    __title = "Seyda Neen Docks"
    MediaTrack.objects.create(
        author=u,
        description='At the docks behind the census and excise office in Seyda Neen, this guy can take you to the lands of <a href="/mods/tamriel-rebuilt/">Tamriel Rebuilt</a> (as well as other destinations.)',
        title=__title,
        url="/images/seyda-neen-docks.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    ).save()

    __title = "East of Balmora"
    MediaTrack.objects.create(
        author=u,
        description="Walking towards Balmora on a dark night.",
        title=__title,
        url="/images/east-of-balmora.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    ).save()

    __title = "Galom Daeus, Entry"
    MediaTrack.objects.create(
        author=u,
        description="""Featuring <a href="/mods/full-dwemer-retexture/">Full Dwemer Retexture</a> and <a href="/mods/dwemer-ruins-retexture/">Dwemer Ruins Retexture</a>.""",
        title=__title,
        url="/images/galom-daeus-entry.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    ).save()

    __title = "Balmora Evening"
    MediaTrack.objects.create(
        author=u,
        description="""Featuring <a href="/mods/balmora-road-normal-maps/">Balmora Road Normal Maps</a>, <a href="/mods/hlaalu-normal-mapped-for-openmw/">Hlaalu Normal Mapped for OpenMW</a>, and <a href="/mods/kilcundas-balmora/">Kilcunda's Balmora</a>.""",
        title=__title,
        url="/images/balmora-evening.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    ).save()

    __title = "Balmora Night"
    MediaTrack.objects.create(
        author=u,
        description="""Also featuring <a href="/mods/balmora-road-normal-maps/">Balmora Road Normal Maps</a>, <a href="/mods/hlaalu-normal-mapped-for-openmw/">Hlaalu Normal Mapped for OpenMW</a>, and <a href="/mods/kilcundas-balmora/">Kilcunda's Balmora</a>.""",
        title=__title,
        url="/images/balmora-night.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    ).save()

    __title = "Scamp Attack"
    MediaTrack.objects.create(
        author=u,
        description='Getting attacked by a little devil, featuring <a href="/mods/scamp-replacer/">Scamp Replacer</a>.',
        title=__title,
        url="/images/scamp-attack.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    ).save()

    __title = "Sadrith Mora"
    MediaTrack.objects.create(
        author=u,
        description='Featuring <a href="/mods/telvanni-retexture/">Telvanni Retexture</a>.',
        title=__title,
        url="/images/sadrith-mora.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    ).save()

    __title = "Swamp Life"
    MediaTrack.objects.create(
        author=u,
        description='On the road to a very swampy place, walking away from <a href="http://en.uesp.net/wiki/Morrowind:Khartag_Point">Khartag Point</a> (yes, I did just acquire a certain special sword..)',
        title=__title,
        url="/images/swamp-life.png",
        slug=slugify(__title),
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-08-05 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="Dark and scenic temple at Ghostgate.",
        title="Temple",
        url="/images/temple.png",
        slug="temple",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="Tough decisions to be made on where to go... and nice-looking signposts via <a href='/mods/signposts-retextured/'>Signposts Retextured</a>.",
        title="Traveling Choices",
        url="/images/traveling-choices.png",
        slug="traveling-choices",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="Tel Branora on a semi cloudy day, with all the ground-covering foliage your heart could desire (via <a href='/mods/vurts-groundcover-for-openmw/'>Vurts Groundcover for OpenMW</a>.)",
        title="Tel Branora, groundcovered",
        url="/images/tel-branora-groundcovered.png",
        slug="tel-branora-groundcovered",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="<a href='https://www.nexusmods.com/morrowind/users/4381248'>PeterBitt</a>'s excellent <a href='/mods/soulgem-replacer/'>Soulgem Replacer</a> in action, looking great.",
        title="Soul Gem Replacer in action",
        url="/images/soulgem-replacer-tf.png",
        slug="soulgem-replacer-in-action",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="The entrance to Nchuleft, featuring <a href='/mods/full-dwemer-retexture/'>Full Dwemer Retexture</a>, <a href='/mods/dwemer-ruins-retexture/'>Dwemer Ruins Retexture</a>, and <a href='/mods/dwemer-mesh-improvement/'>Dwemer Mesh Improvement</a> among others.",
        title="Nchuleft Entrance",
        url="/images/nchuleft-entrance.png",
        slug="nchuleft-entrance",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="Local tradehouse - watercolored.  Using all mods tagged '<a href=\"/mods/tag/expanded-vanilla/\">Expanded Vanilla</a>', plus <a href='/mods/morrowind-watercolored-v2/'>Morrowind Watercolored v2</a> as the main texture pack.",
        title="Local tradehouse: Expanded Vanilla + Morrowind Watercolored v2",
        url="/images/tradehouse-watercolored.png",
        slug="local-tradehouse-essentials-plus-mw-watercolored",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="Local tradehouse - pixelated.  Using all mods tagged '<a href=\"/mods/tag/expanded-vanilla/\">Expanded Vanilla</a>', plus <a href='/mods/pixelwind-v2/'>Pixelwind V2</a> as the main texture pack.",
        title="Local tradehouse: Expanded Vanilla + Pixelwind V2",
        url="/images/tradehouse-pixelated.png",
        slug="local-tradehouse-essentials-plus-pixelwind-v2",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="The Seyda Neen welcoming committee - watercolored.  Using all mods tagged '<a href=\"/mods/tag/expanded-vanilla/\">Expanded Vanilla</a>', plus <a href='/mods/morrowind-watercolored-v2/'>Morrowind Watercolored v2</a> as the main texture pack.",
        title="Seyda Neen welcoming committee: Expanded Vanilla + Morrowind Watercolored v2",
        url="/images/seyda-neen-watercolored.png",
        slug="sn-welc-committee-essentials-plus-mw-watercolored",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="The Seyda Neen welcoming committee - pixelated.  Using all mods tagged '<a href=\"/mods/tag/expanded-vanilla/\">Expanded Vanilla</a>', plus <a href='/mods/pixelwind-v2/'>Pixelwind V2</a> as the main texture pack.",
        title="Seyda Neen welcoming committee: Expanded Vanilla + Pixelwind V2",
        url="/images/seyda-neen-pixelated.png",
        slug="sn-welcoming-committee-essentials-plus-pixelwind",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="Loading into Seyda Neen.  Using all mods tagged '<a href=\"/mods/tag/expanded-vanilla/\">Expanded Vanilla</a>', plus <a href='/mods/morrowind-watercolored-v2/'>Morrowind Watercolored v2</a> as the main texture pack.",
        title="Loading in: Expanded Vanilla + Morrowind Watercolored v2",
        url="/images/MW-Watercolored-v2-1.png",
        slug="loading-in-essentials-plus-mw-watercolored-v2",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    MediaTrack.objects.create(
        author=u,
        description="Loading into Seyda Neen.  Using all mods tagged '<a href=\"/mods/tag/expanded-vanilla/\">Expanded Vanilla</a>', plus <a href='/mods/pixelwind-v2/'>Pixelwind V2</a> as the main texture pack.",
        title="Loading in: Expanded Vanilla + Pixelwind V2",
        url="/images/pixelwindv2-1.png",
        slug="loading-in-essentials-plus-pixelwind-v2",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-07-04 18:00:00 -0500", datetime_format),
    ).save()

    # MediaTrack.objects.create(
    #     author=u,
    #     description="Want to know what the game might look like if you use every mod in this site's recommended list?  Look no further!  This video showcases the first 15 minutes of the game with a fully loaded mod list.",
    #     title="First 15 minutes of gameplay",
    #     url="/videos/openmw-first-15-minutes.mp4",
    #     slug="first-15-minutes-of-gameplay",
    #     media_type=MediaTrack.VIDEO,
    #     status=MediaTrack.LIVE,
    #     date_added=datetime.strptime("2018-06-30 18:00:00 -0500", datetime_format),
    # ).save()

    MediaTrack.objects.create(
        author=u,
        description="Tel Branora at nighttime.",
        title="Tel Branora Night",
        url="/images/tel-branora-night.png",
        slug="tel-branora-night",
        media_type=MediaTrack.IMAGE,
        status=MediaTrack.LIVE,
        date_added=datetime.strptime("2018-06-30 18:00:00 -0500", datetime_format),
    ).save()
