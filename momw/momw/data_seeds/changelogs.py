import glob
import os
import pathlib
import sys

try:
    from packaging.version import Version
except ModuleNotFoundError:
    from distutils.version import StrictVersion as Version
from momw.helpers import read_toml_data
from momw.models import Changelog, ChangelogEntry, ModList


kind_map = {
    "added": ChangelogEntry.ADDED,
    "removed": ChangelogEntry.REMOVED,
    "updated": ChangelogEntry.UPDATED,
}


def changelogs(root_dir="momw/momw/data_seeds/data/changelogs"):
    ihvdc = ModList.objects.get(slug="i-heart-vanilla-directors-cut")
    modlist_map = {
        "i-heart-vanilla": ModList.objects.get(slug="i-heart-vanilla"),
        "i-heart-vanilla-dc": ihvdc,
        "i-heart-vanilla-directors-cut": ihvdc,
        "one-day-morrowind-modernization": ModList.objects.get(
            slug="one-day-morrowind-modernization"
        ),
        "graphics-overhaul": ModList.objects.get(slug="graphics-overhaul"),
        "total-overhaul": ModList.objects.get(slug="total-overhaul"),
        "expanded-vanilla": ModList.objects.get(slug="expanded-vanilla"),
        "starwind-modded": ModList.objects.get(slug="starwind-modded"),
        "just-good-morrowind": ModList.objects.get(slug="just-good-morrowind"),
    }
    cl_dict = {}
    # TODO: use the root_dir keyword arg for glob when the server can support it
    os.chdir(pathlib.Path(__file__).parent.resolve() / "data/changelogs")
    for f in glob.glob("*.toml"):
        cl_dict.update({f.replace(".toml", ""): f})
    sorted_cls = sorted(cl_dict, key=Version, reverse=True)
    for version in sorted_cls:
        tml = cl_dict[version]
        data = read_toml_data(version)
        c = Changelog(version=version)
        c.save()
        for entry in data["log_entry"]:
            modlists = []
            # :todd:
            for ml in entry["modlists"]:
                modlists.append(modlist_map[ml])
            entry["changelog_id"] = c.pk
            try:
                entry["kind"] = kind_map[entry["kind"].lower()]
            except KeyError:
                print(f"Invalid entry kind in {tml}: {entry['kind']}")
                sys.exit(1)
            entry.pop("modlists")
            ce = ChangelogEntry(**entry)
            ce.save()
            for ml in modlists:
                ce.modlists.add(ml)
            ce.save()
