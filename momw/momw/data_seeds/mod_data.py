from momw.helpers import generate_mod, read_toml_data
from momw.models import Category, Tag

IGNORED_CATEGORIES = ["for-the-brave", "tweaks"]


def mods():
    for category in Category.objects.all():
        if category.slug in IGNORED_CATEGORIES:
            continue
        try:
            mod_data = read_toml_data(category.slug)
            for mod in mod_data["mod_details"]:
                try:
                    tags = Tag.objects.filter(name__in=mod["tags"])
                except KeyError:
                    tags = []

                try:
                    compat = mod["compat"]
                except KeyError:
                    compat = None

                try:
                    date_updated = mod["date_updated"]
                except KeyError:
                    date_updated = None

                try:
                    dl_url = mod["dl_url"]
                except KeyError:
                    dl_url = None

                try:
                    is_active = mod["is_active"]
                except KeyError:
                    is_active = None

                try:
                    is_featured = mod["is_featured"]
                except KeyError:
                    is_featured = None

                try:
                    slug = mod["slug"]
                except KeyError:
                    slug = None

                try:
                    status = mod["status"]
                except KeyError:
                    status = None

                try:
                    url = mod["url"]
                except KeyError:
                    url = None

                generate_mod(
                    author=mod["author"],
                    category=category,
                    compat=compat,
                    date_added=mod["date_added"],
                    date_updated=date_updated,
                    description=mod["description"],
                    dl_url=dl_url,
                    is_active=is_active,
                    is_featured=is_featured,
                    name=mod["name"],
                    slug=slug,
                    status=status,
                    tags=tags,
                    url=url,
                )

        except SystemExit as e:
            print(e)
