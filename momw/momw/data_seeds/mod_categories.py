from momw.helpers import generate_category
from momw.models import Category


def mod_cats() -> bool:
    # print()
    # print("Generating Category objects ...", end="")

    fixes = {
        "title": "Fixes",
        "slug": "fixes",
        "description": "Mods that fix things, not just make then nicer.",
    }
    Category(**fixes).save()

    objects_and_clutter = {
        "title": "Objects/Clutter",
        "slug": "objects-and-clutter",
        "description": "Mods that change the various objects and things spread throughout the game world.",
    }
    Category(**objects_and_clutter).save()

    architecture = {
        "title": "Architecture",
        "slug": "architecture",
        "description": "Mods that change arcitecture and housing.",
    }
    Category(**architecture).save()

    npcs = {
        "title": "NPCs",
        "slug": "npcs",
        "description": "Change NPC appearance and bodies.",
    }
    Category(**npcs).save()

    clothing = {
        "title": "Clothing",
        "slug": "clothing",
        "description": "Mods that improve the game's clothing.",
    }
    Category(**clothing).save()

    weapons = {
        "title": "Weapons",
        "slug": "weapons",
        "description": "Mods that improve the weapon meshes and textures.",
    }
    Category(**weapons).save()

    armor = {
        "title": "Armor",
        "slug": "armor",
        "description": "Mods that improve the armor meshes and textures.",
    }
    Category(**armor).save()

    creatures = {
        "title": "Creatures",
        "slug": "creatures",
        "description": "Mods that improve or replace creatures.",
    }
    Category(**creatures).save()

    flora = {
        "title": "Flora",
        "slug": "flora",
        "description": "Mods that alter plantlife and trees.",
    }
    Category(**flora).save()

    landscape = {
        "title": "Landscape",
        "slug": "landscape",
        "description": "Mods that alter the landscape in general including rocks.",
    }
    Category(**landscape).save()

    generate_category(
        **{
            "title": "Skies",
            "slug": "skies",
            "description": "Make the skies more beautiful.",
        }
    )

    user_interface = {
        "title": "User Interface",
        "slug": "user-interface",
        "description": "Mods that change the UI.",
    }
    Category(**user_interface).save()

    gameplay = {
        "title": "Gameplay",
        "slug": "gameplay",
        "description": "Mods that affect gameplay.  Some or all of these might be considered optional but they are all highly recommended.",
    }
    Category(**gameplay).save()

    land = {
        "title": "Landmasses",
        "slug": "landmasses",
        "description": "Landmass additions.",
    }
    Category(**land).save()

    generate_category(
        **{
            "title": "Quests",
            "slug": "quests",
            "description": "Mods that add more quests.",
        }
    )

    guilds_and_factions = {
        "title": "Guilds/Factions",
        "slug": "guilds-and-factions",
        "description": "Mods that add guilds or factions.",
    }
    Category(**guilds_and_factions).save()

    player_homes = {
        "title": "Player Homes",
        "slug": "player-homes",
        "description": "Optional but good to have - places to call home!",
    }
    Category(**player_homes).save()

    audio = {
        "title": "Audio",
        "slug": "audio",
        "description": "Mods that change or add to the audio componenets of the game - including sound FX and music!",
    }
    Category(**audio).save()

    tweaks = {
        "title": "Tweaks",
        "slug": "tweaks",
        "description": "Tweaks for OpenMW itself.",
    }
    Category(**tweaks).save()

    patches = {
        "title": "Patches",
        "slug": "patches",
        "description": "These mods change existing mods, usually for compatibility with another mod or OpenMW itself.",
    }
    Category(**patches).save()

    tes3mp = {
        "title": "TES3MP",
        "slug": "tes3mp",
        "description": "Things to do with TES3MP - be it MP-specific mods/scripts or what have you.",
    }
    Category(**tes3mp).save()

    for_the_brave = {
        "title": "For the brave",
        "slug": "for-the-brave",
        "description": "These mods are ones that are not totally stable with OpenMW.  User beware!",
    }
    Category(**for_the_brave).save()

    generate_category(
        **{
            "title": "Tools",
            "slug": "tools",
            "description": "Tools for doing things and/or making life easier. Not necessarily mods.",
        }
    )

    generate_category(
        **{
            "title": "Cities/Towns",
            "slug": "cities-and-towns",
            "description": "Mods that change or update entire cities (or large parts of them).",
        }
    )

    generate_category(
        **{
            "title": "Videos",
            "slug": "videos",
            "description": "Mods that change or update the few videos/cutscenes that Morrowind sports.",
        }
    )

    generate_category(
        **{
            "title": "Texture Packs",
            "slug": "texture-packs",
            "description": "These don't just replace a few texures, they aim to replace all or large portions of the textures in the game.",
        }
    )

    generate_category(
        **{
            "title": "Collections",
            "slug": "collections",
            "description": "Collections are just that -- a collection of plugins and/or asset replacers which change large portions of the game in one package.",
        }
    )

    generate_category(
        **{
            "title": "Leveling",
            "slug": "leveling",
            "description": "These mods affect the game's leveling systems, including but not limited to how stats are gained/increase (or decrease).",
        }
    )

    generate_category(
        **{
            "title": "Companions",
            "slug": "companions",
            "description": "Mods that add companions, or followers, and other elements of that nature.",
        }
    )

    generate_category(
        **{
            "title": "Mod Managers",
            "slug": "mod-managers",
            "description": "Mod Managers are tools that make installing and organizing mods easier than doing it by hand.",
        }
    )

    generate_category(
        **{
            "title": "Replacers",
            "slug": "replacers",
            "description": "Replacers - usually for meshes but this can also include textures and other assets.",
        }
    )

    generate_category(
        **{
            "title": "Fonts",
            "slug": "fonts",
            "description": "Replacements for the default game font.",
        }
    )

    generate_category(
        **{
            "title": "Water",
            "slug": "water",
            "description": "Things that modify water - be it standing pools, waterfalls, or anything else.",
        }
    )

    generate_category(
        **{
            "title": "Performance",
            "slug": "performance",
            "description": "Asset changes intended to yield a performance boost, and things of that nature.",
        }
    )

    generate_category(
        **{
            "title": "Groundcover",
            "slug": "groundcover",
            "description": 'These mods add "grass" and other foliage to the ground meshes.',
        }
    )

    generate_category(
        **{
            "title": "Shaders",
            "slug": "shaders",
            "description": "Custom shaders that replace the defaults, and more.",
        }
    )

    generate_category(
        **{
            "title": "Modding Resources",
            "slug": "modding-resources",
            "description": "These provide resources that can be used to build mods with, but are not exactly mods themselves.",
        }
    )

    generate_category(
        **{
            "title": "Lighting",
            "slug": "lighting",
            "description": "These mods affect lighting in various ways.",
        }
    )

    generate_category(
        **{
            "title": "Settings Tweaks",
            "slug": "settings-tweaks",
            "description": "Not actually mods, but changes to settings that are built into the OpenMW engine. Enhance performance and visual quality, as well as enable or disable many gameplay options (including things provided by MCP for vanilla Morrowind).",
        }
    )

    generate_category(
        **{
            "title": "Decor",
            "slug": "decor",
            "description": "Add various items to your cells with these mods.",
        }
    )

    generate_category(
        **{
            "title": "Consistency",
            "slug": "consistency",
            "description": "Mods that fix inconsistencies in the Morrowind game world.",
        }
    )

    generate_category(
        **{
            "title": "Camera",
            "slug": "camera",
            "description": "Mods that focus on altering the camera.",
        }
    )

    generate_category(
        **{
            "title": "Animation",
            "slug": "animation",
            "description": "Mods for altering or adding animations.",
        }
    )

    generate_category(
        **{"title": "Trees", "slug": "trees", "description": "Tree replacers."}
    )

    generate_category(
        **{
            "title": "Caves and Dungeons",
            "slug": "caves-and-dungeons",
            "description": "Mods that add, enhance, or replace various caves and dungeons.",
        }
    )

    generate_category(
        **{
            "title": "VFX",
            "slug": "vfx",
            "description": "Mods that add or enhance visual effects.",
        }
    )

    generate_category(
        **{
            "title": "Randomizers",
            "slug": "randomizers",
            "description": "Mods that randomize some aspect of the game.",
        }
    )

    generate_category(
        **{
            "title": "Post Processing Shaders",
            "slug": "post-processing-shaders",
            "description": "Post processing shaders add a variety of nice visual effects.",
        }
    )

    generate_category(
        **{
            "title": "Total Conversions",
            "slug": "total-conversions",
            "description": "All new games made with the Morrowind data files.",
        }
    )

    generate_category(
        **{
            "title": "Combat",
            "slug": "combat",
            "description": "Mods that affect or change the combat mechanics, this can sometimes include things more focused on magic.",
        }
    )

    generate_category(
        **{
            "title": "Books",
            "slug": "books",
            "description": "Mods that add specific books.",
        }
    )

    generate_category(
        **{
            "title": "Original Games",
            "slug": "original-games",
            "description": "All new games that use completely original assets with the OpenMW engine.",
        }
    )

    generate_category(
        **{
            "title": "Wabbajack Lists",
            "slug": "wabbajack-lists",
            "description": "Wabbajack-powered auto-installable mod lists!",
        }
    )

    generate_category(
        **{
            "title": "Weather",
            "slug": "weather",
            "description": "Mods that alter the look and behavior of weather.",
        }
    )

    generate_category(
        **{
            "title": "Travel",
            "slug": "travel",
            "description": "Mods that affect modes of transport.",
        }
    )

    generate_category(
        **{
            "title": "Cut Content",
            "slug": "cut-content",
            "description": "Content that was promised by Todd, but never delivered, reimagined.",
        }
    )

    generate_category(
        **{
            "title": "Magic & Spells",
            "slug": "magic-n-spells",
            "description": "Adding or altering spells in various ways",
        }
    )

    generate_category(
        **{
            "title": "Interiors",
            "slug": "interiors",
            "description": "Mods that alter or add to interior cells.",
        }
    )

    generate_category(
        **{
            "title": "Chargen",
            "slug": "chargen",
            "description": "Mods that alter or affect the character generation portion of the game intro.",
        }
    )
