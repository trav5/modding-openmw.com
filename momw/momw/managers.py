from django.db import models


class TopLevelListsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(parent_list=None)


class UsedCategoriesManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .annotate(num_mods=models.Count("mod"))
            .filter(num_mods__gt=0)
        )


class AltModsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(alt_to__gt=1)


class ListedModsJustGoodMorrowindManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(modlist__slug__startswith="just-good-morrowind")
        )


class ListedModsGraphicsOverhaulManager(models.Manager):
    def get_queryset(self):
        return (
            super().get_queryset().filter(modlist__slug__startswith="graphics-overhaul")
        )


class ListedModsExpandedVanillaManager(models.Manager):
    def get_queryset(self):
        return (
            super().get_queryset().filter(modlist__slug__startswith="expanded-vanilla")
        )


class ListedModsOneDayModernizeManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(modlist__slug__startswith="one-day-morrowind-modernization")
        )


class ListedModsStarwindModdedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(modlist__slug="starwind-modded")


class ListedModsIHeartVanillaManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(modlist__slug="i-heart-vanilla")


class ListedModsIHeartVanillaDCManager(models.Manager):
    def get_queryset(self):
        return (
            super().get_queryset().filter(modlist__slug="i-heart-vanilla-directors-cut")
        )


class ListedModsTotalOverhaulManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(modlist__slug__startswith="total-overhaul")


class GraphicsOverhaulManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(listedmod__modlist__slug__startswith="graphics-overhaul")
        )


class TotalOverhaulManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(listedmod__modlist__slug__startswith="total-overhaul")
        )


class OneDayModernizationManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(
                listedmod__modlist__slug__startswith="one-day-morrowind-modernization"
            )
        )


class LatestModsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by("-date_updated")


class AndroidSwitchManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(listedmod__modlist__slug="androidswitch")


class ExpandedVanillaManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(listedmod__modlist__slug__startswith="expanded-vanilla")
        )


class StarwindModdedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(listedmod__modlist__slug="starwind-modded")


class IHeartVanillaManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(listedmod__modlist__slug="i-heart-vanilla")


class IHeartVanillaDCManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(listedmod__modlist__slug="i-heart-vanilla-directors-cut")
        )


class JustGoodMorrowindManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(listedmod__modlist__slug="just-good-morrowind")
        )


class WithPluginsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by("name").filter(plugins__isnull=False)


class NoPluginsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by("name").filter(plugins__isnull=True)


class CompatFullyWorkingManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(compat=4)


class CompatPartialWorkingManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(compat=5)


class CompatNotWorkingManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(compat=6)


class CompatUnknownManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(compat=7)


class ModPluginsManagerTotalOverhaul(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(on_lists__slug="total-overhaul")


class ExtraCfgManagerTotalOverhaulNoSettings(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(on_lists__slug="total-overhaul", in_settings=False)
        )


class ExtraCfgManagerTotalOverhaulSettings(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(on_lists__slug="total-overhaul", in_settings=True)
        )


class ModPluginsManagerGraphicsOverhaul(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(on_lists__slug="graphics-overhaul")


class ModPluginsManagerExpandedVanilla(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(on_lists__slug="expanded-vanilla")


class ModPluginsManagerIHeartVanilla(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(on_lists__slug="i-heart-vanilla")


class ModPluginsManagerIHeartVanillaDC(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(on_lists__slug="i-heart-vanilla-directors-cut")
        )


class ModPluginsManager1DayModernize(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(on_lists__slug="one-day-morrowind-modernization")
        )


class ModPluginsManagerJustGoodMorrowind(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(on_lists__slug="just-good-morrowind")


class ModPluginsManagerStarwind(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(on_lists__slug="starwind-modded")


class ModPluginsManagerContent(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_bsa=False).filter(is_groundcover=False)


class ModPluginsManagerBSA(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_bsa=True)


class ModPluginsManagerGroundcover(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_groundcover=True)


class ModPluginsManagerNeedsCleaning(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(needs_cleaning=True)
