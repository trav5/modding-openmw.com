{% extends 'base.html' %}
{% block title %}Tips: Creating Custom Groundcover{% endblock %}
{% block content %}
  <h3 class="center">Tips: <span class="bold">Creating Custom Groundcover</span></h3>

  {% include "tips-nav.html" with cur_page="tips-creating-custom-groundcover" %}

  <p>
    The nature of how MGE:XE groundcover (also known as "legacy groundcover") works creates problems when you use any mod which changes the landscape. You'll end up with missing or floating groundcover items. It is possible to generate custom groundcover for your load order, but it is not a trivial process.
  </p>

  <h2>Table of Contents</h2>

  <ul>
    <li><a href="#the-process">The process</a></li>
    {# <li><a href="#merging">Merging</a></li> #}
    <li><a href="#review">Review</a></li>
    <li><a href="#caveats">Caveats</a></li>
    <li><a href="#credits">Credits</a></li>
  </ul>

  <h3 id="the-process"><a href="#the-process">The process</a></h3>

  <p>
    Below I will describe the process I used to create <a href="/mods/momw-aesthesia-groundcover-reworked-patches/">the custom groundcover plugins I provide for this website's mod lists</a>.
  </p>

  <ol>
    <li>Make sure you've got a functional install of vanilla Morrowind available.</li>
    <li>Make sure you've got a functional install of <a href="/mods/tes3cmd/">TES3CMD</a>.</li>
    <li>Find all plugins that contain <code>LAND</code> records:
      <ul>
        <li>Using the recommended folder structure from this website makes it somewhat easy to do with some shell scripting. This is the fish shell snippet I used for finding them in my own setup:</li>
        <pre><code>#!/usr/bin/fish
for p in (find $HOME/games/openmw/Mods -type f -iname "*.esp")
    tes3cmd dump --list "$p" 2>/dev/null | grep -aq LAND; and echo "$p"
 end</code></pre>
      </ul>
    </li>
    <li>Of the results obtained from the previous step, examine each to determine which need to be included for the purpose of generating grass.
      <ul>
        <li>This step requires you to know a bit about the mod you're looking at, but most times it should be obvious if you should include a mod or not.</li>
        <li>For example, <a href="/mods/tamriel-rebuilt/">Tamriel Rebuilt</a> might come up as containing <code>LAND</code> records, but nothing on this site's mod lists change those outside of it, so there's no need to recreate the groundcover. But something like <a href="/mods/trackless-grazelands/">Trackless Grazelands</a> does affect the landscape in a way that affects groundcover (you'd have conspicuous missing groundcover where the roads used to be).</li>
      </ul>
    </li>
    <li>Create a fake cfg file with just the plugins you need, each one per line with <code>content=</code> just like in a real <code>openmw.cfg</code> file. Here's mine:</li>
    <pre><code>content=OAAB_Data.esm
content=Solstheim Tomb of The Snow Prince.esm
content=Patch for Purists.esm
content=PortsOfVvardenfellV1.6-TR_Travels-TOTSP-Ald-ruhn-Vivec-City.esp
content=Unofficial Morrowind Official Plugins Patched.ESP
content=Clean_DA_Sobitur_Facility_Clean.ESP
content=Clean_DA_Sobitur_Repurposed_1.ESP
content=The Rise of the Tribe Unmourned.esp
content=Concept_Arts_plantations.esp
content=Clean_Mines & Caverns.esp
content=Clean_Foyada Mamaea Overhaul.ESP
content=Vurt's Grazelands Unique Trees.ESP
content=OAAB_Grazelands.ESP
content=Trackless Grazeland.ESP
content=WhiteSuran2_MD_Edition.esp
content=Clean_Stav_gnisis_minaret.ESP
content=RR_Telvanni_Lighthouse_Tel Vos_Eng.ESP
content=RR_Telvanni_Lighthouse_Tel Branora_Eng.ESP
content=RR_Holamayan_Eng.ESP
content=RR_Ghost_Gate_Fortress_Eng.ESP
content=DD_Caldera_Expansion.esp
content=Clean_Ald-ruhn.ESP
content=Clean_Hla Oad.esp
content=Seyda_Neen_Gateway.ESP
content=Clean_Vivec City.esp</code></pre>
    <li>Convert this to Morrowind's ini file format. I wrote a python script that does this, you can find it <a href="https://git.sr.ht/~hristoast/openmwcfg2mwini#openmwcfg2mwini">here</a>. My converted file looks like this:</li>
    <pre><code>[Game Files]
GameFile0=Morrowind.esm
GameFile1=Tribunal.esm
GameFile2=Bloodmoon.esm
GameFile3=OAAB_Data.esm
GameFile4=Solstheim Tomb of The Snow Prince.esm
GameFile5=Patch for Purists.esm
GameFile6=PortsOfVvardenfellV1.6-TR_Travels-TOTSP-Ald-ruhn-Vivec-City.esp
GameFile7=Unofficial Morrowind Official Plugins Patched.ESP
GameFile8=Clean_DA_Sobitur_Facility_Clean.ESP
GameFile9=Clean_DA_Sobitur_Repurposed_1.ESP
GameFile10=The Rise of the Tribe Unmourned.esp
GameFile11=Concept_Arts_plantations.esp
GameFile12=Clean_Mines & Caverns.esp
GameFile13=Clean_Foyada Mamaea Overhaul.ESP
GameFile14=WhiteSuran2_MD_Edition.esp
GameFile15=Clean_Stav_gnisis_minaret.ESP
GameFile16=RR_Telvanni_Lighthouse_Tel Vos_Eng.ESP
GameFile17=RR_Telvanni_Lighthouse_Tel Branora_Eng.ESP
GameFile18=RR_Holamayan_Eng.ESP
GameFile19=RR_Ghost_Gate_Fortress_Eng.ESP
GameFile20=DD_Caldera_Expansion.esp
GameFile21=Clean_Ald-ruhn.ESP
GameFile22=Clean_Hla Oad.esp
GameFile23=Seyda_Neen_Gateway.ESP
GameFile24=Clean_Vivec City.esp
GameFile25=Vurt's Grazelands Unique Trees.ESP
GameFile26=OAAB_Grazelands.ESP
GameFile27=Trackless Grazeland.ESP
GameFile28=OAAB Dwemer Pavements.ESP</code></pre>
    <li>Paste that load order into your vanilla Morrowind install's <code>Morrowind.ini</code> file. Make a backup beforehand, if you wish.</li>
    <li>Read <a href="https://www.project-tamriel.com/viewtopic.php?t=29">this guide</a> to understand what the various options of the mesh generator do. Technically optional, but highly recommended.</li>
    <li>Make sure the configuration ini files for the groundcover you wish to generate are available. In many cases, the mods themselves come with these or have them available as an optional download.</li>
    <li>Run the mesh generator exe.
      <ul>
        <li>Folks that rely on <a href="https://www.winehq.org/">wine</a> need not worry; I generated this website's custom groundcover with wine, it works well enough but is prone to crashing (no different than Windows, unfortunately).</li>
        <li>You want to make <span class="bold">two</span> plugins for each ini config; one for the the modded changes you may have, and another to cover the rest of the untouched vanilla cells.</li>
        <li>When generating the plugins for modded land, you want to exclude the vanilla game <code>.esm</code> files (<code>Morrowind.esm</code>, <code>Tribunal.esm</code>, and <code>Bloodmoon.esm</code>).</li>
        <li>When generating plugins for vanilla content, leave only <code>Morrowind.esm</code> selected for "Data files to search".</li>
      </ul>
    </li>
    <li>In the mesh generator GUI, click "Browse" to find the ini file for the groundcover you wish to generate. Many mods make these available on a per-region basis, and these tend to be less crashy in my experience.</li>
    <li>Morrowind location should auto-fill, if not enter that correctly.</li>
    <li>Give the generated plugin a unique name.</li>
    <li>If generating a plugin for vanilla content, leave only <code>Morrowind.esm</code> selected for "Data files to search".</li>
    <li>If generating a plugin for modded content, click "Import from Ini", and then remove <code>Morrowind.esm</code>, <code>Tribunal.esm</code>, and <code>Bloodmoon.esm</code>.</li>
    <li>Click "Generate".
      <ul>
        <li>For vanilla content, I didn't get many crashes. But when I generated modded content I did seem to get the occasional crash. If this happens, you can simply re-enter everything into the GUI and try again. Sometimes I had to try several times before it worked, don't get discouraged.</li>
      </ul>
    </li>
    <li>With plugins generated for all of your modded and vanilla content, it isn't a bad idea to dump just the cell records into a new plugin for each one. Something like this could be done for each plugin you've generated:</li>
    <pre><code>tes3cmd dump --type cell --raw-with-header momw_AesAdGrass_AC_Grass_cells.esp momw_AesAdGrass_AC_Grass.esp</code></pre>
    <li>Now you must determine which cells are changed by your modded plugins so that you can delete them from your vanilla plugins. This fish shell snippet is what I used:</li>
    <pre><code>#!/usr/bin/fish
for region in "AC" "AI" "AL" "BC" "GL" "WG";
    for cell in (tes3cmd dump --type cell momw_AesAdGrass_"$region"_Grass_cells.esp | grep CELL | awk -F\( '{ print $2 }' | awk -F\) '{ print $1 }');
        echo tes3cmd delete --type cell --id \'\\\($cell\\\)\' mw_AesAdGrass_"$region"_Grass_cells.esp
    end
end</code></pre>
    <li>The snippet from the previous step will print out several <code>tes3cmd delete</code> commands that you can copy and paste into your shell to run. Each one will delete a cell that was found in your modded plugins. Here's a sample of some of the commands I generated and subsequently copy-pasted to run:</li>
    <pre><code>tes3cmd delete --type cell --id '\(2, -12\)' mw_AesAdGrass_AC_Grass_cells.esp
tes3cmd delete --type cell --id '\(12, -8\)' mw_AesAdGrass_AC_Grass_cells.esp
tes3cmd delete --type cell --id '\(12, 14\)' mw_AesAdGrass_AC_Grass_cells.esp</code></pre>
    <li>At this point, you should have fully functional modded and vanilla groundcover plugins.</li>
  </ol>

  <h3 id="review"><a href="#review">Review</a></h3>

  <p>
    A final step would be to run the game with your new plugins and look for errors.  I created the OpenMW-Lua mod <a href="https://git.sr.ht/~hristoast/show-current-cell">show-current-cell</a>, which prints your current cell's name and <code>x, y</code> grid coordinates when you press the <code>=</code> key, to aid in hunting down such problems. This makes it easy to see, in-game, what cell you're in.
  </p>

  <p>
    When you know the cell <code>x, y</code> of where you're having problems, you can go back and look at what TES3CMD reported and find the offender. If you didn't save the output from running TES3CMD, it might be a good idea to redo those steps and save that output.
  </p>

  <h3 id="caveats"><a href="#caveats">Caveats</a></h3>

  <p>
    Of course this process is not perfect. Here's some of what I consider to be its major caveats:
  </p>

  <ul>
    <li>It isn't clear how one can generate modded and vanilla content together. Maybe I've missed some usage for the mesh generator, but I couldn't get it to work, hence my steps above doing them separately.</li>
    <li>I had issues properly generating groundcover for the Grazelands region when three mods affected terrain there. I had to investigate which modded changes to that region I'd skip in favor of vanilla groundcover.</li>
    <li>If there is no way to generate them all together, it'd be nice to at least merge the generated plugins once you're done doing all the manual steps to them. Unfortunately, every time I did this I ended up with missing groundcover that was present pre-merge. As of yet I've been unable to determine what happened in those cases, so my custom plugins are all un-merged.</li>
    <li>Some mods cause the mesh generator to crash and I've not been able to determine why. In this case, I generated grass without considering them and hope things looked okay.</li>
    <li>The plugins I generated with the mesh generator tool lacked <code>STAT</code> records. I'm not sure if this is a feature or a bug.</li>
    <li>There will almost certainly be placement mistakes, so you will want to take a look for things like groundcover seeping into a road, a rock, or other mistakes. I had to manually edit a handful of areas to fix things that were too glaring to be left alone.</li>
  </ul>

  <h3 id="credits"><a href="#credits">Credits</a></h3>

  <p>
    This guide (and, the custom plugins I created for this website) would not be possible without the valuable help of eddie5 and ezze. Thanks a ton!
  </p>

  {% include "tips-nav.html" with cur_page="tips-creating-custom-groundcover" %}

{% endblock %}
