import json

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from ..forms import CfgGeneratorPresetForm
from ..models import DataPath, ExtraCfg, ListedMod, ModList, ModPlugin
from ..helpers import get_visitor_os


def cfg_blank(request, t="cfg_generator.html"):
    return render(request, t, {})


def cfg_generator(request, preset, as_json=False, t="cfg_generator.html"):
    queried = False
    if preset:
        queried = True
    fallback_archives = []
    data_paths = []
    extra_cfg_settings = []
    extra_cfg_nosettings = []
    groundcover = []
    plugins = []
    used_mods = []
    used_mods_by_sublist = {}
    has_openmw_cfg = False
    has_settings_cfg = False
    preset_form = CfgGeneratorPresetForm(request.POST)
    used_select = False
    visitor_os = get_visitor_os(request) or "windows"

    # A preset button was clicked.
    if preset_form.is_valid():
        if request.method == "POST":
            mlist = [*request.POST.dict().keys()][1:]
            yesdev = "yesdev-hidden" in mlist

            if preset == "one day modernize":
                modlist = get_object_or_404(
                    ModList, slug="one-day-morrowind-modernization"
                )
            elif preset == "i heart vanilla dc":
                modlist = get_object_or_404(
                    ModList, slug="i-heart-vanilla-directors-cut"
                )
            else:
                modlist = get_object_or_404(ModList, slug=preset.replace(" ", "-"))
            mlist.insert(0, "morrowind")

            if yesdev:
                plugins = ModPlugin.content.filter(
                    Q(for_mod__slug__in=mlist),
                    Q(on_lists=modlist),
                    Q(dev_build=True) | Q(dev_build=None),
                )
            else:
                plugins = ModPlugin.content.filter(
                    Q(for_mod__slug__in=mlist),
                    Q(on_lists=modlist),
                    Q(dev_build=False) | Q(dev_build=None),
                )

            groundcover = ModPlugin.objects.filter(
                is_groundcover=True, for_mod__slug__in=mlist, on_lists=modlist
            )
            fallback_archives = ModPlugin.objects.filter(
                is_bsa=True, for_mod__slug__in=mlist, on_lists=modlist
            )

            if yesdev:
                data_paths = DataPath.objects.filter(
                    Q(for_mod__slug__in=mlist),
                    Q(on_lists=modlist),
                    Q(for_cfg=True),
                    Q(dev_build=True) | Q(dev_build=None),
                )
            else:
                data_paths = DataPath.objects.filter(
                    Q(for_mod__slug__in=mlist),
                    Q(on_lists=modlist),
                    Q(for_cfg=True),
                    Q(dev_build=False) | Q(dev_build=None),
                )

            extra_cfg_nosettings = ExtraCfg.extra_cfg_nosettings.filter(
                for_mods__slug__in=mlist, on_lists=modlist
            )
            extra_cfg_settings = ExtraCfg.extra_cfg_settings.filter(
                for_mods__slug__in=mlist, on_lists=modlist
            ).order_by("text")

            used_mods_by_sublist = {}
            if modlist.is_parent:
                for sublist in modlist.modlist_set.all():
                    used_mods_by_sublist.update({sublist: []})
                    for m in ListedMod.objects.select_related("mod").filter(
                        modlist=sublist
                    ):
                        used_mods_by_sublist[sublist].append(m)
            else:
                used_mods = ListedMod.objects.filter(modlist__slug=modlist.slug)

            if extra_cfg_settings:
                has_settings_cfg = True
            if (
                extra_cfg_nosettings
                or plugins
                or groundcover
                or fallback_archives
                or data_paths
            ):
                has_openmw_cfg = True

        elif preset:
            modlist = get_object_or_404(ModList, slug=preset)
            use_dev_build = request.GET.get("dev") == "on"

            has_openmw_cfg = True
            has_settings_cfg = True

            if use_dev_build:
                plugins = ModPlugin.objects.filter(
                    Q(is_bsa=False),
                    Q(is_groundcover=False),
                    Q(on_lists=modlist),
                    Q(dev_build=True) | Q(dev_build=None),
                )
            else:
                plugins = ModPlugin.objects.filter(
                    Q(is_bsa=False),
                    Q(is_groundcover=False),
                    Q(on_lists=modlist),
                    Q(dev_build=False) | Q(dev_build=None),
                )

            groundcover = ModPlugin.objects.filter(
                is_groundcover=True, on_lists=modlist
            )
            fallback_archives = ModPlugin.objects.filter(is_bsa=True, on_lists=modlist)

            if use_dev_build:
                data_paths = DataPath.objects.filter(
                    Q(on_lists=modlist),
                    Q(for_cfg=True),
                    Q(dev_build=True) | Q(dev_build=None),
                )
            else:
                data_paths = DataPath.objects.filter(
                    Q(on_lists=modlist),
                    Q(for_cfg=True),
                    Q(dev_build=False) | Q(dev_build=None),
                )

            extra_cfg_nosettings = ExtraCfg.extra_cfg_nosettings.filter(
                on_lists=modlist
            )
            extra_cfg_settings = ExtraCfg.extra_cfg_settings.filter(
                on_lists=modlist
            ).order_by("text")

            # TODO: if it's a list with sublists, send mods grouped by sublist
            used_mods_by_sublist = {}
            if modlist.is_parent:
                for sublist in modlist.modlist_set.all():
                    used_mods_by_sublist.update({sublist: []})
                    for m in ListedMod.objects.select_related("mod").filter(
                        modlist=sublist
                    ):
                        used_mods_by_sublist[sublist].append(m)
            else:
                used_mods = ListedMod.objects.filter(modlist__slug=modlist.slug)

    fallback_values_string = ""
    for thing in extra_cfg_nosettings:
        for mod in thing.for_mods.all():
            fallback_values_string += "#\n# For: {}\n#\n".format(mod)
        fallback_values_string += thing.text

    data_paths_string = ""
    for path in data_paths:
        data_paths_string += 'data="{}"\n'.format(path.for_os(visitor_os))
    data_paths_string = data_paths_string.rstrip()

    fallback_archives_string = ""
    for bsa in fallback_archives:
        fallback_archives_string += "fallback-archive={}\n".format(bsa.file_name)
    fallback_archives_string = fallback_archives_string.rstrip()

    content_files_string = ""
    for plugin in plugins:
        if plugin.needs_cleaning:
            content_files_string += "content=Clean_{}\n".format(plugin.file_name)
        else:
            content_files_string += "content={}\n".format(plugin.file_name)
    content_files_string = content_files_string.rstrip()

    groundcover_files_string = ""
    for plugin in groundcover:
        groundcover_files_string += "groundcover={}\n".format(plugin.file_name)
    groundcover_files_string = groundcover_files_string.rstrip()

    used_settings = []
    if extra_cfg_settings:
        for e in extra_cfg_settings:
            if e.text not in used_settings:
                used_settings.append(e.text)

    settings_cfg_string = "[Lua]\nlua profiler = false\n"
    for block in used_settings:
        for line in block.split("\n"):
            if line not in settings_cfg_string:
                if line.startswith("["):
                    line = "\n" + line
                settings_cfg_string += line
                settings_cfg_string += "\n"

    settings_cfg_string = settings_cfg_string.lstrip().rstrip()

    if as_json:
        response = HttpResponse(
            json.dumps(
                {
                    "openmw_cfg": {
                        "fallback values": fallback_values_string,
                        "data paths": data_paths_string,
                        "fallback archives": fallback_archives_string,
                        "content files": content_files_string,
                        "groundcover files": groundcover_files_string,
                    },
                    "settings_cfg": settings_cfg_string,
                }
            ),
            content_type="application/json",
        )
    else:
        response = render(
            request,
            t,
            {
                "used_select": used_select,
                "preset": preset,
                "preset_title": modlist.title,
                "extra_cfg_nosettings": extra_cfg_nosettings,
                "queried": queried,
                "used_mods": used_mods,
                "has_settings_cfg": has_settings_cfg,
                "has_openmw_cfg": has_openmw_cfg,
                "fallback_values_string": fallback_values_string,
                "data_paths_string": data_paths_string,
                "fallback_archives_string": fallback_archives_string,
                "content_files_string": content_files_string,
                "groundcover_files_string": groundcover_files_string,
                "settings_cfg_string": settings_cfg_string,
                "used_mods_by_sublist": used_mods_by_sublist,
                "visitor_os": visitor_os,
            },
        )
    return response
