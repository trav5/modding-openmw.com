from django.db.models import Q
from django.shortcuts import render

from ..models import Mod


def search(request):
    q = request.GET.get("q", "").strip(" ")
    results_mods = []
    if q:
        if len(q) <= 2:
            results_mods = Mod.objects.filter(abbrev_name__icontains=q).order_by("name")
        else:
            try:
                # remove special characters
                results_mods = Mod.objects.filter(
                    Q(name__icontains=q)
                    | Q(abbrev_name__icontains=q)
                    | Q(isalpha_name__icontains=q)
                    | Q(special_to_spaces_name__icontains=q)
                    | Q(author__icontains=q)
                    | Q(description__icontains=q)
                ).order_by("name")
            except Mod.DoesNotExist:
                pass
    return render(request, "search.html", {"q": q, "results_mods": results_mods})
