from django.conf import settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from ..models import ModList


# https://selenium-python.readthedocs.io/api.html#module-selenium.webdriver.common.action_chains
# https://docs.djangoproject.com/en/5.0/topics/testing/tools/#django.test.LiveServerTestCase
class MomwSeleniumFF(StaticLiveServerTestCase):
    fixtures = ["test-data.json"]  # Usually created with `make dumpdata`

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.all_mod_list_slugs = []
        cls.title_suffix = " | Modding OpenMW: A guide to modding and modernizing Morrowind with OpenMW"
        # Do this so static files load correctly
        settings.DEBUG = True
        for ml in ModList.objects.all():
            if not ml.parent_list and ml.slug != "tes3mp-server":
                cls.all_mod_list_slugs.append(ml.slug)
        cls.ffdriver = webdriver.Firefox()
        cls.ffdriver.implicitly_wait(10)  # Seconds

    @classmethod
    def tearDownClass(cls):
        cls.ffdriver.quit()
        super().tearDownClass()

    def test_index(self):
        self.ffdriver.get(f"{self.live_server_url}/")
        for text in (
            "A website dedicated to modding and modernizing Morrowind with",
            "Start Here",
            "Choose a mod list:",
            "Connect:",
            "What's New:",
        ):
            self.assertTrue(text in self.ffdriver.page_source)

    def test_webmanifest(self):
        self.ffdriver.get(f"{self.live_server_url}/manifest.webmanifest")
        self.assertTrue(
            "A website dedicated to modding and modernizing Morrowind with OpenMW!"
            in self.ffdriver.page_source
        )

    def test_changelogs(self):
        self.ffdriver.get(f"{self.live_server_url}/changelogs")
        self.assertEqual(
            self.ffdriver.title,
            f"Mod List Changelogs{self.title_suffix}",
        )

    def test_changelogs_website(self):
        self.ffdriver.get(f"{self.live_server_url}/changelogs/website")
        self.assertEqual(
            self.ffdriver.title,
            f"Website Changelog{self.title_suffix}",
        )

    def test_compatibility(self):
        self.ffdriver.get(f"{self.live_server_url}/compatibility")
        self.assertEqual(
            self.ffdriver.title,
            f"Mod Compatibility{self.title_suffix}",
        )
        self.ffdriver.get(f"{self.live_server_url}/compat/fully-working")
        self.assertEqual(
            self.ffdriver.title,
            f"Compatibility: Fully Working{self.title_suffix}",
        )
        self.ffdriver.get(f"{self.live_server_url}/compat/partially-working")
        self.assertEqual(
            self.ffdriver.title,
            f"Compatibility: Partially Working{self.title_suffix}",
        )
        self.ffdriver.get(f"{self.live_server_url}/compat/not-working")
        self.assertEqual(
            self.ffdriver.title,
            f"Compatibility: Not Working{self.title_suffix}",
        )
        self.ffdriver.get(f"{self.live_server_url}/compat/unknown")
        self.assertEqual(
            self.ffdriver.title,
            f"Compatibility: Unknown{self.title_suffix}",
        )

    def test_settings(self):
        self.ffdriver.get(f"{self.live_server_url}/settings")
        self.assertEqual(
            self.ffdriver.title,
            f"User Settings{self.title_suffix}",
        )

    def test_subscribe(self):
        self.ffdriver.get(f"{self.live_server_url}/subscribe")
        self.assertEqual(
            self.ffdriver.title,
            f"Subscribe{self.title_suffix}",
        )

    def test_cookie(self):
        self.ffdriver.get(f"{self.live_server_url}/cookie")
        self.assertEqual(
            self.ffdriver.title,
            f"Cookie Policy{self.title_suffix}",
        )

    def test_enable_in_cfg_generator_button(self):
        self.ffdriver.get(f"{self.live_server_url}/enable-in-cfg-generator-button")
        self.assertEqual(
            self.ffdriver.title,
            f'Enable In CFG Generator Button "HOW TO" Page{self.title_suffix}',
        )

    def test_faq(self):
        self.ffdriver.get(f"{self.live_server_url}/faq")
        self.assertEqual(
            self.ffdriver.title,
            f"FAQ{self.title_suffix}",
        )

    def test_faq_tooling(self):
        self.ffdriver.get(f"{self.live_server_url}/faq/tooling")
        self.assertEqual(
            self.ffdriver.title,
            f"FAQ: Tooling{self.title_suffix}",
        )

    def test_getting_started(self):
        self.ffdriver.get(f"{self.live_server_url}/getting-started")
        self.assertEqual(
            self.ffdriver.title,
            f"Setup Steps{self.title_suffix}",
        )

    def test_getting_started_buy(self):
        self.ffdriver.get(f"{self.live_server_url}/getting-started/buy")
        self.assertEqual(
            self.ffdriver.title,
            f"Getting Started: Buy Morrowind{self.title_suffix}",
        )

    def test_getting_started_install(self):
        self.ffdriver.get(f"{self.live_server_url}/getting-started/install")
        self.assertEqual(
            self.ffdriver.title,
            f"Getting Started: Install OpenMW{self.title_suffix}",
        )

    def test_getting_started_settings(self):
        self.ffdriver.get(f"{self.live_server_url}/getting-started/settings")
        self.assertEqual(
            self.ffdriver.title,
            f"Getting Started: Tweak Settings{self.title_suffix}",
        )

    def test_getting_started_tips(self):
        self.ffdriver.get(f"{self.live_server_url}/getting-started/tips")
        self.assertEqual(
            self.ffdriver.title,
            f"Getting Started: Tips{self.title_suffix}",
        )

    def test_guides(self):
        self.ffdriver.get(f"{self.live_server_url}/guides")
        self.assertEqual(
            self.ffdriver.title,
            f"Guides{self.title_suffix}",
        )

    def test_guides_developers(self):
        self.ffdriver.get(f"{self.live_server_url}/guides/developers")
        self.assertEqual(
            self.ffdriver.title,
            f"Developers' Guide{self.title_suffix}",
        )

    def test_guides_modders(self):
        self.ffdriver.get(f"{self.live_server_url}/guides/modders")
        self.assertEqual(
            self.ffdriver.title,
            f"Modders' Guide{self.title_suffix}",
        )

    # TODO: Test the folder deploy script download
    def test_guides_users(self):
        self.ffdriver.get(f"{self.live_server_url}/guides/users")
        self.assertEqual(
            self.ffdriver.title,
            f"Users' Guide{self.title_suffix}",
        )

    def test_lists(self):
        self.ffdriver.get(f"{self.live_server_url}/lists")
        self.assertEqual(
            self.ffdriver.title,
            f"Curated Mod Lists{self.title_suffix}",
        )

    def test_lists_add(self):
        self.ffdriver.get(f"{self.live_server_url}/lists/add")
        self.assertEqual(
            self.ffdriver.title,
            f"How To Become A Mod List Curator{self.title_suffix}",
        )

    # TODO: Mod list detail page tests

    def test_load_order(self):
        self.ffdriver.get(f"{self.live_server_url}/load-order")
        self.assertEqual(
            self.ffdriver.title,
            f"Load Order{self.title_suffix}",
        )

    # TODO: Media

    def test_mods(self):
        self.ffdriver.get(f"{self.live_server_url}/mods")
        self.assertEqual(
            self.ffdriver.title,
            f"All Mods{self.title_suffix}",
        )

    def test_mods_all(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/all")
        self.assertEqual(
            self.ffdriver.title,
            f"All Mods{self.title_suffix}",
        )

    def test_mods_alts(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/alts")
        self.assertEqual(
            self.ffdriver.title,
            f"Alternative Mods{self.title_suffix}",
        )

    def test_category_all(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/category/all")
        self.assertEqual(
            self.ffdriver.title,
            f"Mod Categories{self.title_suffix}",
        )

    # TODO: Category detail tests

    def test_mods_latest(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/latest")
        self.assertEqual(
            self.ffdriver.title,
            f"Latest 100 Mod Additions/Updates{self.title_suffix}",
        )

    def test_mods_tag(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/tag")
        self.assertEqual(
            self.ffdriver.title,
            f"Mod Tags{self.title_suffix}",
        )

    # TODO: Tag detail tests

    def test_mod_no_plugin(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/no-plugin")
        self.assertEqual(
            self.ffdriver.title,
            f"Mods Not Requiring A Plugin{self.title_suffix}",
        )

    def test_mod_with_plugin(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/with-plugin")
        self.assertEqual(
            self.ffdriver.title,
            f"Mods Requiring A Plugin{self.title_suffix}",
        )

    def test_example_mod_page(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/example-mod-page")
        self.assertEqual(
            self.ffdriver.title,
            f"How to use Modding-OpenMW.com{self.title_suffix}",
        )

    def test_go_home_mod_page(self):
        self.ffdriver.get(f"{self.live_server_url}/mods/go-home")
        self.assertEqual(
            self.ffdriver.title,
            f"Go Home!{self.title_suffix}",
        )

    # TODO: test sublist has parent list data
    def test_mod_list_final(self):
        for ml in self.all_mod_list_slugs:
            self.ffdriver.get(f"{self.live_server_url}/lists/{ml}/final")
            self.assertTrue("Final Steps: " in self.ffdriver.title)

    # TODO: FF can't open Atom feeds :(
    # def test_feeds_mods(self):
    #     self.ffdriver.get(f"{self.live_server_url}/feeds/mods")
    #     self.assertTrue("<title>Mod Feed</title>" in self.ffdriver.page_source)

    def test_cfg_generator(self):
        self.ffdriver.get(f"{self.live_server_url}/cfg-generator")
        self.assertEqual(
            self.ffdriver.title,
            f"CFG Generator{self.title_suffix}",
        )

    # for preset in (
    #     "expanded-vanilla",
    #     "graphics-overhaul",
    #     "i-heart-vanilla",
    #     "i-heart-vanilla-directors-cut",
    #     "just-good-morrowind",
    #     "one-day-morrowind-modernization",
    #     "starwind-modded",
    #     "total-overhaul",
    # ):
    #     self.ffdriver.get(f"{self.live_server_url}/cfg/{preset}")
    #     # TODO: find something on the page
