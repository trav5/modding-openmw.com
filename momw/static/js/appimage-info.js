/*
  momw - appimage-info.js
  Copyright (C) 2022  Modding-OpenMW.com Project

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const OpenMWdevVersion = "0.49.0";

function getLatest() {
    const req = new Request("/files/appimages/dev/latest.json");
    fetch(req)
        .then(function(response) {
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            return response.json();
        })
        .then(function(data) {
            document.getElementById("openmw-commit-link").href = `https://gitlab.com/OpenMW/openmw/-/commit/${data.sha}`;
            document.getElementById("openmw-commit-link").textContent = data.sha;
            document.getElementById("devCS").href = `/files/appimages/dev/OpenMW_Content_Editor-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage`;
            document.getElementById("devCS-sig").href = `/files/appimages/dev/OpenMW_Content_Editor-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage.minisig`;
            document.getElementById("devEngine").href = `/files/appimages/dev/OpenMW_Engine-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage`;
            document.getElementById("devEngine-sig").href = `/files/appimages/dev/OpenMW_Engine-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage.minisig`;
            document.getElementById("devLauncher").href = `/files/appimages/dev/OpenMW_Launcher-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage`;
            document.getElementById("devLauncher-sig").href = `/files/appimages/dev/OpenMW_Launcher-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage.minisig`;
            document.getElementById("devBSAtool").href = `/files/appimages/dev/OpenMW_BSAtool-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage`;
            document.getElementById("devBSAtool-sig").href = `/files/appimages/dev/OpenMW_BSAtool-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage.minisig`;
            document.getElementById("devESMtool").href = `/files/appimages/dev/OpenMW_ESMtool-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage`;
            document.getElementById("devESMtool-sig").href = `/files/appimages/dev/OpenMW_ESMtool-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage.minisig`;
            document.getElementById("devINIimporter").href = `/files/appimages/dev/OpenMW_INI_Importer-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage`;
            document.getElementById("devINIimporter-sig").href = `/files/appimages/dev/OpenMW_INI_Importer-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage.minisig`;
            document.getElementById("devNavmeshtool").href = `/files/appimages/dev/OpenMW_Navmeshtool-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage`;
            document.getElementById("devNavmeshtool-sig").href = `/files/appimages/dev/OpenMW_Navmeshtool-${OpenMWdevVersion}_dev_${data.date}_${data.shortsha}-x86_64.AppImage.minisig`;
        })
}

window.onload = getLatest;
