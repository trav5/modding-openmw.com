from django import template

register = template.Library()


@register.filter(name="plus_one")
def plus_one(number):
    return number + 1


@register.filter(name="minus_one_or_none")
def minus_one_or_none(number):
    n = number - 1
    if n <= 0:
        return None
    else:
        return n


@register.filter(name="times")
def times(number):
    if number:
        return range(1, number + 1)
    else:
        return [0, 0]
