"""
Useful functions and etc. for working with querysets.  Cut the boilerplate!
"""

from django.conf import settings
from django.core.paginator import Paginator
from itertools import chain
from operator import attrgetter


def merge_querysets(*querysets, key="date_added", reverse=True):
    """
    Takes two 'querysets' and merges them on a common 'key', regardless of the
    underlying model.  Useful for merging selects of two models with a common
    field, like a date field of some kind.
    """
    return sorted(chain(*querysets), key=attrgetter(key), reverse=reverse)


def paginate_queryset(request=None, queryset=None, count=10):
    """
    Given a 'request' and a 'queryset', paginate by 'count'.

    Intended to wrap some of the boilerplate of doing pagination.
    """
    paginator = Paginator(queryset, settings.PAGINATE_BY)
    page = request.GET.get("page")
    return paginator.get_page(page)
