# Generated by Django 3.2.10 on 2023-11-05 04:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('chroniko', '0002_alter_blogtag_name_alter_blogtag_slug_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogtag',
            name='name',
            field=models.CharField(max_length=100, unique=True, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='blogtag',
            name='slug',
            field=models.SlugField(max_length=100, unique=True, verbose_name='Slug'),
        ),
        migrations.AlterField(
            model_name='taggedentry',
            name='content_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='chroniko_taggedentry_tagged_items', to='contenttypes.contenttype', verbose_name='Content type'),
        ),
        migrations.AlterField(
            model_name='taggedentry',
            name='object_id',
            field=models.IntegerField(db_index=True, verbose_name='Object id'),
        ),
    ]
